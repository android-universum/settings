/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting.test;

import android.content.Context;
import android.view.View;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceViewHolder;
import androidx.recyclerview.widget.RecyclerView;

import static org.mockito.Mockito.mock;

/**
 * @author Martin Albedinsky
 */
public final class TestPreferenceViewHolders {

    private TestPreferenceViewHolders() {
        // Not allowed to be instantiated publicly.
        throw new UnsupportedOperationException();
    }

    @NonNull public static PreferenceViewHolder createMockHolder(@NonNull final Context context) throws Exception {
        return createMockHolder(new View(context));
    }

    @NonNull public static PreferenceViewHolder createMockHolder(@NonNull final View itemView) throws Exception {
        final PreferenceViewHolder mockHolder = mock(PreferenceViewHolder.class);
        final Field itemViewField = RecyclerView.ViewHolder.class.getField("itemView");
        itemViewField.setAccessible(true);
        itemViewField.set(mockHolder, itemView);
        return mockHolder;
    }
}