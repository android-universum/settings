/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.SeekBar;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.VisibleForTesting;
import androidx.preference.PreferenceViewHolder;

/**
 * A {@link SettingPreference} implementation that provides {@link SeekBar} widget with its related
 * functionality.
 * <p>
 * This preference implementation by default displays the preferred progress via {@link SeekBar}
 * widget in the area of the standard summary text (instead of it). The preferred progress may be
 * specified via {@link #setProgress(int)} and obtained via {@link #getProgress()}. Also the maximum
 * progress value that should be handled by the associated SeekBar widget may be specified via
 * {@link #setMaxProgress(int)}.
 *
 * <h3>Default value</h3>
 * Default value for this preference is parsed as {@link Integer}. See {@link TypedArray#getInt(int, int)}.
 *
 * <h3>Xml attributes</h3>
 * See {@link SettingPreference},
 * {@link R.styleable#SettingSeekBarPreference SettingSeekBarPreference Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingSeekBarPreferenceStyle settingSeekBarPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("RedundantCast")
public class SettingSeekBarPreference extends SettingPreference {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingSeekBarPreference";

	/**
	 * Default maximum value for slider's progress.
	 */
	@VisibleForTesting static final int DEFAULT_MAX_PROGRESS = 100;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Listener that is used to receive callbacks about changed progress in the SeekBar widget of
	 * this preference.
	 */
	private final SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {

		/**
		 */
		@Override public void onProgressChanged(@NonNull final SeekBar seekBar, final int progress, final boolean fromUser) {
			// Ignored. See onStopTrackingTouch(...).
		}

		/**
		 */
		@Override public void onStartTrackingTouch(@NonNull final SeekBar seekBar) {
			// Ignored. See onStopTrackingTouch(...).
		}

		/**
		 */
		@Override public void onStopTrackingTouch(@NonNull final SeekBar seekBar) {
			final int newProgress = seekBar.getProgress();
			if (newProgress != getProgress() && callChangeListener(newProgress)) {
				setProgress(newProgress);
			}
		}
	};

	/**
	 * Maximum value for the progress that may be specified for this preference. This maximum value
	 * is used only to specified max value for {@link SeekBar} widget associated with this preference
	 * via {@link SeekBar#setMax(int)}.
	 *
	 * @see #onBindViewHolder(PreferenceViewHolder)
	 */
	private int maxProgress = DEFAULT_MAX_PROGRESS;

	/**
	 * Boolean flag indicating whether the progress value for this preference has been set or not.
	 * This flag is used to handle case when the same value is being specified for this preference,
	 * but for the first time, to properly refresh view of this preference and notify listeners about
	 * the change.
	 */
	private boolean progressSet;

	/**
	 * Current progress value specified for this preference. This may be either value specified by
	 * the user, default value or persisted value.
	 */
	private int progress;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingSeekBarPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingSeekBarPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingSeekBarPreference(Context, AttributeSet, int)} with
	 * {@link R.attr#settingSeekBarPreferenceStyle} as attribute for default style.
	 */
	public SettingSeekBarPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingSeekBarPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingSeekBarPreference(Context, AttributeSet, int, int)} with {@code 0}
	 * as default style.
	 */
	public SettingSeekBarPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of SettingSeekBarPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingSeekBarPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this setting preference to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this preference specific data from it that can be used to configure this new preference instance.
	 * The specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default
	 * data from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingSeekBarPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingSeekBarPreference_android_max) {
				this.maxProgress = attributes.getInt(attrIndex, maxProgress);
			}
		}
		attributes.recycle();
	}

	/**
	 */
	@Override protected Object onGetDefaultValue(@NonNull final TypedArray attributes, final int index) {
		return attributes.getInt(index, progress);
	}

	/**
	 */
	@Override protected void onSetInitialValue(@Nullable final Object defaultValue) {
		setProgress(getPersistedInt(defaultValue == null ? 0 :(int) defaultValue));
	}

	/**
	 * Sets a maximum value for the progress that may be specified for this preference.
	 * <p>
	 * <b>Note</b>, that this value is not used to ensure that the progress specified via {@link #setProgress(int)}
	 * is in bounds of the maximum progress value, but only for purpose of {@link SeekBar#setMax(int)}.
	 *
	 * @param maxProgress The desired maximum progress value.
	 *
	 * @see android.R.attr#max
	 * @see #getMaxProgress()
	 */
	public void setMaxProgress(final int maxProgress) {
		this.maxProgress = maxProgress;
	}

	/**
	 * Returns the maximum value for progress specified for this preference.
	 *
	 * @return Maximum progress value.
	 * @see #setMaxProgress(int)
	 */
	public int getMaxProgress() {
		return maxProgress;
	}

	/**
	 * Sets a preferred progress value for this preference.
	 * <p>
	 * If value of this preference changes, it is persisted and the change listener is notified
	 * about the change.
	 *
	 * @param progress The preferred progress to be persisted. Should be from the range {@code [0, getMaxProgress()]}.
	 *
	 * @see #getProgress()
	 */
	public void setProgress(final int progress) {
		final boolean changed = this.progress != progress;
		if (changed || !progressSet) {
			this.progress = Math.min(progress, maxProgress);
			this.progressSet = true;
			persistInt(this.progress);
			if (changed) {
				notifyChanged();
			}
		}
	}

	/**
	 * Returns the preferred progress value of this preference.
	 *
	 * @return Progress either specified by the user, as default value or the persisted one.
	 *
	 * @see #setProgress(int)
	 */
	public int getProgress() {
		return progress;
	}

	/**
	 */
	@Override public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);
		holder.itemView.setClickable(false);
		final SeekBar seekBar = (SeekBar) holder.findViewById(R.id.setting_seek_bar);
		if (seekBar != null) {
			seekBar.setOnSeekBarChangeListener(null);
			seekBar.setMax(maxProgress);
			seekBar.setProgress(progress);
			seekBar.setOnSeekBarChangeListener(listener);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}