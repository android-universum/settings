/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.res.TypedArray;
import android.os.Build;
import android.widget.SeekBar;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingSeekBarPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingSeekBarPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingSeekBarPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context(), null, R.attr.settingSeekBarPreferenceStyle);
		// Assert:
		assertThat(preference.getMaxProgress(), is(SettingSeekBarPreference.DEFAULT_MAX_PROGRESS));
		assertThat(preference.getProgress(), is(0));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context(), null, R.attr.settingSeekBarPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getMaxProgress(), is(SettingSeekBarPreference.DEFAULT_MAX_PROGRESS));
		assertThat(preference.getProgress(), is(0));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getInt(0, preference.getProgress())).thenReturn(50);
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is(50));
		verify(mockTypedArray).getInt(0, preference.getProgress());
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		// Act + Assert:
		preference.onSetInitialValue(25);
		assertThat(preference.getProgress(), is(25));
		preference.onSetInitialValue(75);
		assertThat(preference.getProgress(), is(75));
	}

	@Test public void testMaxProgress() {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		// Act + Assert:
		assertThat(preference.getMaxProgress(), is(SettingSeekBarPreference.DEFAULT_MAX_PROGRESS));
		preference.setMaxProgress(200);
		assertThat(preference.getMaxProgress(), is(200));
	}

	@Test public void testProgress() {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		final Preference.OnPreferenceChangeListener mockListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockListener);
		// Act + Assert:
		assertThat(preference.getProgress(), is(0));
		preference.setProgress(55);
		assertThat(preference.getProgress(), is(55));
		preference.setProgress(55);
		assertThat(preference.getProgress(), is(55));
		preference.setProgress(101);
		assertThat(preference.getProgress(), is(100));
		verifyZeroInteractions(mockListener);
	}

	@Test public void testOnBindView() throws Exception {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		preference.setMaxProgress(200);
		preference.setProgress(50);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final SeekBar mockSeekBar = mock(SeekBar.class);
		when(mockViewHolder.findViewById(R.id.setting_seek_bar)).thenReturn(mockSeekBar);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_seek_bar);
		verify(mockSeekBar).setOnSeekBarChangeListener(null);
		verify(mockSeekBar).setMax(200);
		verify(mockSeekBar).setProgress(50);
		verify(mockSeekBar).setOnSeekBarChangeListener(any(SeekBar.OnSeekBarChangeListener.class));
		verifyNoMoreInteractions(mockSeekBar);
	}

	@Test public void testOnBindViewWithoutSeekBarInHierarchy() throws Exception {
		// Arrange:
		final SettingSeekBarPreference preference = new SettingSeekBarPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		when(mockViewHolder.findViewById(R.id.setting_seek_bar)).thenReturn(null);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_seek_bar);
	}
}