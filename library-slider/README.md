Settings-Slider
===============

This module contains preference implementation that allows to set a desired setting value via 
**slider** widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-slider:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

Below are listed some of **primary elements** that are available in this module:

- [SettingSeekBarPreference](https://bitbucket.org/android-universum/settings/src/main/library-slider/src/main/java/universum/studios/android/setting/SettingSeekBarPreference.java)
