Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/settings/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/settings/src/main/library-core)**
- **[Fragment](https://bitbucket.org/android-universum/settings/src/main/library-fragment)**
- **[Menu](https://bitbucket.org/android-universum/settings/src/main/library-menu)**
- **[Selection](https://bitbucket.org/android-universum/settings/src/main/library-selection)**
- **[Slider](https://bitbucket.org/android-universum/settings/src/main/library-slider)**
- **[Empty](https://bitbucket.org/android-universum/settings/src/main/library-empty)**
- **[@Dialog](https://bitbucket.org/android-universum/settings/src/main/library-dialog_group)**
- **[Dialog-Base](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base)**
- **[Dialog-Collection](https://bitbucket.org/android-universum/settings/src/main/library-dialog-collection)**
- **[Dialog-DateTime](https://bitbucket.org/android-universum/settings/src/main/library-dialog-datetime)**
- **[Dialog-Edit](https://bitbucket.org/android-universum/settings/src/main/library-dialog-edit)**
- **[Dialog-Color](https://bitbucket.org/android-universum/settings/src/main/library-dialog-color)**
