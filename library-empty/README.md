Settings-Empty
===============

This module contains implementation of preference that may be used to display an **empty** space
in settings list hierarchy in order to support **animations** in a setting preference that is placed
at the top of the list.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-empty:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

Below are listed some of **primary elements** that are available in this module:

- [SettingEmptyPreference](https://bitbucket.org/android-universum/settings/src/main/library-empty/src/main/java/universum/studios/android/setting/SettingEmptyPreference.java)
