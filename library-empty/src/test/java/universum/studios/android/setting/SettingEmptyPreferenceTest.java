/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Build;

import org.junit.Test;
import org.robolectric.annotation.Config;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SettingEmptyPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingEmptyPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingEmptyPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingEmptyPreference preference = new SettingEmptyPreference(context(), null, 0);
		// Assert:
		assertThat(preference.isEnabled(), is(false));
		assertThat(preference.isSelectable(), is(false));
		assertThat(preference.isPersistent(), is(false));
		assertThat(preference.getLayoutResource(), is(R.layout.setting_preference_empty));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingEmptyPreference preference = new SettingEmptyPreference(context(), null, 0, 0);
		// Assert:
		assertThat(preference.isEnabled(), is(false));
		assertThat(preference.isSelectable(), is(false));
		assertThat(preference.isPersistent(), is(false));
		assertThat(preference.getLayoutResource(), is(R.layout.setting_preference_empty));
	}
}