Settings-Menu
===============

This module contains preference implementation that allows to set a single desired setting value that
may be picked from multiple values displayed in a **menu** widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-menu:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

Below are listed some of **primary elements** that are available in this module:

- [SettingSpinnerPreference](https://bitbucket.org/android-universum/settings/src/main/library-menu/src/main/java/universum/studios/android/setting/SettingSpinnerPreference.java)
