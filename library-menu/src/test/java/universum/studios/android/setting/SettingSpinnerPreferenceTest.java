/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.app.Activity;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingSpinnerPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingSpinnerPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingSpinnerPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context(), null, R.attr.settingSpinnerPreferenceStyle);
		// Assert:
		assertThat(preference.getEntries(), is(nullValue()));
		assertThat(preference.getEntryValues(), is(nullValue()));
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context(), null, R.attr.settingSpinnerPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getEntries(), is(nullValue()));
		assertThat(preference.getEntryValues(), is(nullValue()));
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getString(0)).thenReturn("TestValue.1");
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is("TestValue.1"));
		verify(mockTypedArray).getString(0);
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		// Act + Assert:
		preference.onSetInitialValue("TestValue.1");
		assertThat(preference.getValue(), is("TestValue.1"));
		preference.onSetInitialValue("TestValue.2");
		assertThat(preference.getValue(), is("TestValue.2"));
	}

	@Test public void testSetTitle() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		// Act:
		preference.setTitle("Test title");
		// Assert:
		assertThat(preference.getTitle(), is("Test title"));
	}

	@Test public void testEntries() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2"};
		// Act:
		preference.setEntries(testEntries);
		final CharSequence[] entries = preference.getEntries();
		// Assert:
		assertThat(entries, is(testEntries));
	}

	@Test public void testEntriesFromResourceId() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final CharSequence[] testEntries = context().getResources().getTextArray(android.R.array.emailAddressTypes);
		// Act:
		preference.setEntries(android.R.array.emailAddressTypes);
		final CharSequence[] entries = preference.getEntries();
		// Assert:
		assertThat(entries, is(testEntries));
	}

	@Test public void testEntryValues() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		// Act:
		preference.setEntryValues(testValues);
		final String[] values = preference.getEntryValues();
		// Assert:
		assertThat(values, is(testValues));
	}

	@Test public void testEntryValuesFromResourceId() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = context().getResources().getStringArray(android.R.array.emailAddressTypes);
		// Act:
		preference.setEntryValues(android.R.array.emailAddressTypes);
		final String[] values = preference.getEntryValues();
		// Assert:
		assertThat(values, is(testValues));
	}

	@Test public void testValueIndex() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		preference.setEntryValues(testValues);
		// Act + Assert:
		preference.setValueIndex(0);
		assertThat(preference.getValueIndex(), is(0));
		assertThat(preference.getValue(), is("TestValue.1"));
		preference.setValueIndex(1);
		assertThat(preference.getValueIndex(), is(1));
		assertThat(preference.getValue(), is("TestValue.2"));
	}

	@Test public void testValueIndexThatIsGreaterThanValuesArray() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		preference.setEntryValues(testValues);
		// Act + Assert:
		preference.setValueIndex(2);
		assertThat(preference.getValueIndex(), is(SettingSpinnerPreference.EntriesAdapter.NO_POSITION));
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Test public void testValueIndexWithoutValues() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		// Act:
		preference.setValueIndex(1);
		// Assert:
		assertThat(preference.getValueIndex(), is(SettingSpinnerPreference.EntriesAdapter.NO_POSITION));
		assertThat(preference.getValue(), is(nullValue()));
	}

	@Test public void testValue() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		preference.setEntryValues(testValues);
		// Act + Assert:
		preference.setValue("TestValue.1");
		assertThat(preference.getValue(), is("TestValue.1"));
		assertThat(preference.getValueIndex(), is(0));
		preference.setValue("TestValue.2");
		preference.setValue("TestValue.2");
		assertThat(preference.getValue(), is("TestValue.2"));
		assertThat(preference.getValueIndex(), is(1));
	}

	@Test public void testOnBindView() throws Exception {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		preference.setEntryValues(testValues);
		preference.setValue("TestValue.2");
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockViewHolder.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_spinner);
		verify(mockSpinner).setOnItemSelectedListener(null);
		verify(mockSpinner).setAdapter(any(SpinnerAdapter.class));
		verify(mockSpinner).setSelection(1, false);
		verify(mockSpinner).setOnItemSelectedListener(any(AdapterView.OnItemSelectedListener.class));
		verifyNoMoreInteractions(mockSpinner);
	}

	@Test public void testOnBindViewWithoutValueSet() throws Exception {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockViewHolder.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_spinner);
		verify(mockSpinner).setOnItemSelectedListener(null);
		verify(mockSpinner).setAdapter(any(SpinnerAdapter.class));
		verify(mockSpinner).setOnItemSelectedListener(any(AdapterView.OnItemSelectedListener.class));
		verifyNoMoreInteractions(mockSpinner);
	}

	@Test public void testOnBindViewWithoutWidgetInHierarchy() throws Exception {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		when(mockViewHolder.findViewById(R.id.setting_spinner)).thenReturn(null);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_spinner);
	}


	@Test public void testOnClick() throws Exception {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockViewHolder.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		preference.onBindViewHolder(mockViewHolder);
		// Act:
		preference.onClick();
		// Assert:
		verify(mockSpinner).performClick();
	}

	@Test public void testOnClickWithoutBoundView() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final View mockView = mock(View.class);
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockView.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		// Act:
		preference.onClick();
	}

	@Test public void testOnPrepareForRemoval() throws Exception {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockViewHolder.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		preference.onBindViewHolder(mockViewHolder);
		// Act:
		preference.onPrepareForRemoval();
		// Assert:
		verify(mockSpinner, times(2)).setOnItemSelectedListener(null);
	}

	@Test public void testOnPrepareForRemovalWithoutBoundView() {
		// Arrange:
		final SettingSpinnerPreference preference = new SettingSpinnerPreference(context());
		final View mockView = mock(View.class);
		final Spinner mockSpinner = mock(Spinner.class);
		when(mockView.findViewById(R.id.setting_spinner)).thenReturn(mockSpinner);
		// Act:
		preference.onPrepareForRemoval();
	}

	@Test public void testEntriesAdapterInstantiation() {
		// Act:
		new SettingSpinnerPreference.EntriesAdapter(context());
	}

	@Test public void testEntriesAdapterSetTitle() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		final DataSetObserver mockObserver = mock(DataSetObserver.class);
		adapter.registerDataSetObserver(mockObserver);
		// Act + Assert:
		adapter.setTitle("Test title - 1");
		verify(mockObserver).onChanged();
		adapter.setTitle("Test title - 1");
		verify(mockObserver, times(1)).onChanged();
		adapter.setTitle("Test title - 2");
		verify(mockObserver, times(2)).onChanged();
		verifyNoMoreInteractions(mockObserver);
	}

	@Test public void testEntriesAdapterSetViewLayoutResource() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		final DataSetObserver mockObserver = mock(DataSetObserver.class);
		adapter.add("Test entry - 1");
		adapter.registerDataSetObserver(mockObserver);
		// Act + Assert:
		adapter.setViewLayoutResource(android.R.layout.simple_list_item_1);
		verify(mockObserver).onChanged();
		verifyNoMoreInteractions(mockObserver);
	}

	@Test public void testEntriesAdapterSetViewLayoutResourceWithoutEntries() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		final DataSetObserver mockObserver = mock(DataSetObserver.class);
		adapter.registerDataSetObserver(mockObserver);
		// Act + Assert:
		adapter.setViewLayoutResource(android.R.layout.simple_list_item_1);
		verifyZeroInteractions(mockObserver);
	}

	@Test public void testEntriesAdapterSetDropDownViewLayoutResource() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		final DataSetObserver mockObserver = mock(DataSetObserver.class);
		adapter.add("Test entry - 1");
		adapter.registerDataSetObserver(mockObserver);
		// Act + Assert:
		adapter.setDropDownViewLayoutResource(android.R.layout.simple_spinner_dropdown_item);
		verify(mockObserver).onChanged();
		verifyNoMoreInteractions(mockObserver);
	}

	@Test public void testEntriesAdapterSetDropDownViewLayoutResourceWithoutEntries() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		final DataSetObserver mockObserver = mock(DataSetObserver.class);
		adapter.registerDataSetObserver(mockObserver);
		// Act + Assert:
		adapter.setDropDownViewLayoutResource(android.R.layout.simple_spinner_dropdown_item);
		verifyZeroInteractions(mockObserver);
	}

	@Test public void testEntriesAdapterGetView() {
		// Arrange:
		final Activity activity = Robolectric.buildActivity(TestActivity.class).create().get();
		activity.setTheme(R.style.Setting_Theme);
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(activity);
		adapter.setTitle("Test title");
		adapter.add("Test entry - 1");
		// Act:
		final View view = adapter.getView(0, null, new FrameLayout(context()));
		// Assert:
		assertThat(view, is(notNullValue()));
		assertThat(view.getTag(), is(notNullValue()));
		assertThat(view.findViewById(android.R.id.title), is(notNullValue()));
		assertThat(((TextView) view.findViewById(android.R.id.title)).getText(), is("Test title"));
		assertThat(view.findViewById(android.R.id.summary), is(notNullValue()));
		assertThat(((TextView) view.findViewById(android.R.id.summary)).getText(), is("Test entry - 1"));
	}

	@Test public void testEntriesAdapterGetDropDownView() {
		// Arrange:
		final SettingSpinnerPreference.EntriesAdapter adapter = new SettingSpinnerPreference.EntriesAdapter(context());
		adapter.add("Test entry - 1");
		// Act:
		final View view = adapter.getDropDownView(0, null, new FrameLayout(context()));
		// Assert:
		assertThat(view, is(notNullValue()));
		assertThat(view.getTag(), is(notNullValue()));
		assertThat(view.findViewById(android.R.id.text1), is(notNullValue()));
		assertThat(((TextView) view.findViewById(android.R.id.text1)).getText(), is("Test entry - 1"));
	}
}