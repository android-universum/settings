/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.setting.ui.fragment;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.samples.setting.R;
import universum.studios.android.setting.SettingDialogPreferenceManager;
import universum.studios.android.setting.SettingsBaseFragment;

/**
 * @author Martin Albedinsky
 */
public final class PreviewSettingsFragment extends SettingsBaseFragment
		implements
		Preference.OnPreferenceChangeListener,
		Dialog.OnDialogListener {

	private static final String TAG = "PreviewSettingsFragment";

	private SettingDialogPreferenceManager dialogPreferencesManager;

	@Override public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getPreferenceScreen().setOnPreferenceChangeListener(this);
		this.dialogPreferencesManager = new SettingDialogPreferenceManager(this);
		this.dialogPreferencesManager.attachToPreferenceScreen(getPreferenceScreen());
	}

	@Override public void onCreatePreferences(@Nullable final Bundle savedInstanceState, @Nullable final String rootKey) {
		addPreferencesFromResource(R.xml.settings_preview);
	}

	@Override public boolean onPreferenceChange(@NonNull final Preference preference, final Object newValue) {
		Log.d(TAG, "onPreferenceChange(preference: " + preference.getKey() + ", newValue: " + newValue + ")");
		return true;
	}

	@Override public boolean onDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
		return dialogPreferencesManager.handlePreferenceDialogButtonClick(dialog, button);
	}

	@Override public void onDestroy() {
		super.onDestroy();
		this.dialogPreferencesManager.detachFromPreferenceScreen(getPreferenceScreen());
		getPreferenceScreen().setOnPreferenceChangeListener(null);
	}
}