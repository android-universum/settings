/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.setting.ui;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import universum.studios.android.samples.setting.R;
import universum.studios.android.setting.SettingHeadersAdapter;
import universum.studios.android.setting.SettingsBaseActivity;

/**
 * @author Martin Albedinsky
 */
public final class SettingsActivity extends SettingsBaseActivity {

	@Override protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addToolbar();
		final ListAdapter headersAdapter = getHeadersAdapter();
		if (headersAdapter instanceof SettingHeadersAdapter) {
			((SettingHeadersAdapter) headersAdapter).setUseVectorIcons(true);
		}
	}

	@Override @NonNull protected Toolbar onCreateToolbar(@NonNull final LayoutInflater inflater, @NonNull final ViewGroup root) {
		return (Toolbar) inflater.inflate(R.layout.samples_toolbar, root, false);
	}

	@Override public void onBuildHeaders(@NonNull final List<PreferenceActivity.Header> target) {
		super.onBuildHeaders(target);
		loadHeadersFromResource(R.xml.settings, target);
	}

	@Override public void onHeaderClick(@NonNull final Header header, final int position) {
		Toast.makeText(this, "Not supported for PreferenceActivity from JetPack!", Toast.LENGTH_SHORT).show();
	}

	@Override protected boolean isValidFragment(@NonNull final String fragmentName) { return false; }
}