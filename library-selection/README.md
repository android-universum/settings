Settings-Selection
===============

This module contains preference implementations that allow to set a desired setting value via widget
that supports **selection**, like **check box** or **radio button**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-selection:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

Below are listed some of **primary elements** that are available in this module:

- [SettingCheckBoxPreference](https://bitbucket.org/android-universum/settings/src/main/library-selection/src/main/java/universum/studios/android/setting/SettingCheckBoxPreference.java)
- [SettingSwitchPreference](https://bitbucket.org/android-universum/settings/src/main/library-selection/src/main/java/universum/studios/android/setting/SettingSwitchPreference.java)
