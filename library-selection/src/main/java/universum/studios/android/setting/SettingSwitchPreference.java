/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.Switch;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.preference.PreferenceViewHolder;
import androidx.preference.SwitchPreference;

/**
 * Extended {@link SwitchPreference} that provides additional features supported by the <b>Settings</b>
 * library.
 *
 * <h3>Xml attributes</h3>
 * See {@link SwitchPreference}
 *
 * <h3>Default style attribute</h3>
 * {@link android.R.attr#switchPreferenceStyle android:switchPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SettingSwitchPreference extends SwitchPreference {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingSwitchPreference";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Decorator used to extend API of this setting preference by functionality otherwise not supported
	 * or not available due to current API level.
	 */
	private PreferenceDecorator decorator;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingSwitchPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingSwitchPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingSwitchPreference(Context, AttributeSet, int)} with
	 * {@link android.R.attr#switchPreferenceStyle} as attribute for default style.
	 */
	public SettingSwitchPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, android.R.attr.switchPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingSwitchPreference(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public SettingSwitchPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.ensureDecorator();
	}

	/**
	 * Creates a new instance of SettingSwitchPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingSwitchPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.ensureDecorator();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Ensures that the decorator for this view is initialized.
	 */
	private void ensureDecorator() {
		if (decorator == null) this.decorator = new PreferenceDecorator(this) {

			/**
			 */
			@Override @Nullable Object onGetDefaultValue(@NonNull final TypedArray attributes, final int index) {
				return SettingSwitchPreference.this.onGetDefaultValue(attributes, index);
			}

			/**
			 */
			@Override void onUpdateInitialValue(@Nullable final Object defaultValue) {
				SettingSwitchPreference.this.onSetInitialValue(defaultValue);
			}
		};
	}

	/**
	 */
	@Override public void setKey(@NonNull final String key) {
		final boolean changed = !key.equals(getKey());
		super.setKey(key);
		if (changed) {
			this.ensureDecorator();
			this.decorator.handleKeyChange();
		}
	}

	/**
	 */
	@Override public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);
		this.ensureDecorator();
		this.decorator.onBindViewHolder(holder);
		final View checkableView = holder.findViewById(R.id.setting_switch);
		if (checkableView instanceof Checkable) {
			if (checkableView instanceof Switch) {
				final Switch switchView = (Switch) checkableView;
				switchView.setOnCheckedChangeListener(null);
			}
			((Checkable) checkableView).setChecked(isChecked());
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}