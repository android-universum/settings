/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Build;
import android.widget.Switch;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingSwitchPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingSwitchPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingSwitchPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		new SettingSwitchPreference(context(), null, android.R.attr.switchPreferenceStyle);
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		new SettingSwitchPreference(context(), null, android.R.attr.switchPreferenceStyle, 0);
	}

	@Test public void testSetKey() {
		// Arrange:
		final SettingSwitchPreference preference = new SettingSwitchPreference(context());
		// Act + Assert:
		preference.setKey("KEY.Test.1");
		assertThat(preference.getKey(), is("KEY.Test.1"));
		preference.setKey("KEY.Test.2");
		preference.setKey("KEY.Test.2");
		assertThat(preference.getKey(), is("KEY.Test.2"));
	}

	@Test public void testOnBindViewHolder() throws Exception {
		// Arrange:
		final SettingSwitchPreference preference = new SettingSwitchPreference(context());
		preference.setChecked(true);
		preference.setChecked(true);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final Switch mockSwitch = mock(Switch.class);
		when(mockViewHolder.findViewById(R.id.setting_switch)).thenReturn(mockSwitch);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
		verify(mockViewHolder).findViewById(R.id.setting_switch);
		verify(mockSwitch).setChecked(true);
	}

	@Test public void testOnBindViewHolderWithoutWidgetInHierarchy() throws Exception {
		// Arrange:
		final SettingSwitchPreference preference = new SettingSwitchPreference(context());
		preference.setChecked(true);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		when(mockViewHolder.findViewById(R.id.setting_switch)).thenReturn(null);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.icon_frame);
	}
}