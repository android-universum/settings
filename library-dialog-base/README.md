Settings-Dialog-Base
===============

This module contains base elements for **dialog** related setting preferences.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-dialog-base:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core),
[universum.studios.android:dialogs](https://bitbucket.org/android-universum/dialogs)

Below are listed some of **primary elements** that are available in this module:

- [SettingDialogPreference](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base/src/main/java/universum/studios/android/setting/SettingDialogPreference.java)
- [SettingDialogPreferencesManager](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base/src/main/java/universum/studios/android/setting/SettingDialogPreference.java)
