/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingDialogPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingDialogPreference preference = new SettingDialogPreference(context(), null, android.R.attr.dialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingDialogPreference preference = new SettingDialogPreference(context(), null, android.R.attr.dialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
	}

	@Test public void testOnCreateDialogOptions() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		// Act:
		final DialogOptions options = preference.onCreateDialogOptions(context().getResources());
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is(nullValue()));
		assertThat(options.content(), is(nullValue()));
		assertThat(options, is(not(preference.onCreateDialogOptions(context().getResources()))));
	}

	@Test public void setOnClickListener() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		final SettingDialogPreference.OnClickListener mockListener = mock(SettingDialogPreference.OnClickListener.class);
		// Act:
		preference.setOnClickListener(mockListener);
		preference.setOnClickListener(null);
		// Assert:
		verifyZeroInteractions(mockListener);
	}

	@Test public void testSynchronizeSummaryView() throws Exception {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setSummary("Test summary.");
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.synchronizeSummaryView(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.summary);
		verifyNoMoreInteractions(mockViewHolder);
		verify(mockSummaryView).setText("Test summary.");
		verify(mockSummaryView).setVisibility(View.VISIBLE);
		verifyNoMoreInteractions(mockSummaryView);
	}

	@Test public void testSynchronizeSummaryViewWithoutSummaryText() throws Exception {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.synchronizeSummaryView(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.summary);
		verifyNoMoreInteractions(mockViewHolder);
		verify(mockSummaryView).setVisibility(View.GONE);
		verifyNoMoreInteractions(mockSummaryView);
	}

	@Test public void testSynchronizeSummaryViewWithoutSummaryViewInHierarchy() throws Exception {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setSummary("Test summary.");
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(null);
		// Act:
		preference.synchronizeSummaryView(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(android.R.id.summary);
		verifyNoMoreInteractions(mockViewHolder);
	}

	@Test public void testOnGetSummaryText() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setSummary("Test summary.");
		// Act:
		final CharSequence summary = preference.onGetSummaryText();
		// Assert:
		assertThat(summary, is("Test summary."));
	}

	@Test public void testDialogId() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		// Act + Assert:
		preference.setDialogId(100);
		assertThat(preference.getDialogId(), is(100));
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		// Act:
		final DialogOptions options = preference.getDialogOptions();
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is(nullValue()));
		assertThat(options.content(), is(nullValue()));
	}

	@Test public void testHandleDialogButtonClick() {
		// Arrange:
		final TestPreference preference = new TestPreference(context());
		preference.setDialogId(5);
		final Dialog mockDialog = mock(Dialog.class);
		when(mockDialog.getDialogId()).thenReturn(5);
		// Act:
		final boolean result = preference.handleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE);
		// Assert:
		assertThat(result, is(true));
		verify(mockDialog).getDialogId();
		assertThat(preference.onHandleDialogButtonClickInvoked, is(true));
	}

	@Test public void testHandleDialogButtonClickForDialogNotAssociatedWithPreference() {
		// Arrange:
		final TestPreference preference = new TestPreference(context());
		preference.setDialogId(5);
		final Dialog mockDialog = mock(Dialog.class);
		when(mockDialog.getDialogId()).thenReturn(7);
		// Act:
		final boolean result = preference.handleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE);
		// Assert:
		assertThat(result, is(false));
		verify(mockDialog).getDialogId();
		assertThat(preference.onHandleDialogButtonClickInvoked, is(false));
	}

	@Test public void testOnHandleDialogButtonClick() {
		// Arrange:
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mock(Dialog.class), Dialog.BUTTON_POSITIVE), is(false));
	}

	static final class TestPreference extends SettingDialogPreference {

		boolean onHandleDialogButtonClickInvoked;

		TestPreference(@NonNull final Context context) {
			super(context);
		}

		@Override protected boolean onHandleDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
			this.onHandleDialogButtonClickInvoked = true;
			return true;
		}
	}
}