/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Bundle;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.robolectric.Robolectric;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.dialog.manage.DialogFactory;
import universum.studios.android.dialog.manage.DialogXmlFactory;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public final class SettingDialogPreferenceManagerTest extends AndroidTestCase {

	private PreferenceScreen createPreferenceScreen() {
		return createPreferenceFragment().getPreferenceManager().createPreferenceScreen(context());
	}

	private TestPreferenceFragment createPreferenceFragment() {
		final FragmentActivity activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
		final TestPreferenceFragment fragment = new TestPreferenceFragment();
		activity.getSupportFragmentManager().beginTransaction().add(fragment, null).commitAllowingStateLoss();
		return fragment;
	}

	@Test public void testInstantiation() {
		// Act:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		// Assert:
		assertThat(manager.getDialogController(), is(nullValue()));
		assertThat(manager.getDialogFactory(), is(nullValue()));
	}

	@Test public void testInstantiationForActivity() {
		// Arrange:
		final TestActivity mockActivity = mock(TestActivity.class);
		// Act:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager(mockActivity);
		// Assert:
		assertThat(manager.getDialogController(), is(notNullValue()));
		assertThat(manager.getDialogFactory(), is(notNullValue()));
		assertThat(manager.getDialogFactory(), instanceOf(DialogXmlFactory.class));
	}

	@Test public void testInstantiationForFragment() {
		// Arrange:
		final TestFragment mockFragment = mock(TestFragment.class);
		final TestActivity mockActivity = mock(TestActivity.class);
		when(mockFragment.requireContext()).thenReturn(mockActivity);
		// Act:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager(mockFragment);
		// Assert:
		assertThat(manager.getDialogController(), is(notNullValue()));
		assertThat(manager.getDialogFactory(), is(notNullValue()));
		assertThat(manager.getDialogFactory(), instanceOf(DialogXmlFactory.class));
	}

	@Test public void testInstantiationForFragmentManager() {
		// Arrange:
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		// Act:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager(mockFragmentManager);
		// Assert:
		assertThat(manager.getDialogController(), is(notNullValue()));
		assertThat(manager.getDialogFactory(), is(nullValue()));
	}

	@Test public void testDialogControllerWithoutFactorySpecified() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogFactory mockFactory = mock(DialogFactory.class);
		manager.setDialogFactory(mockFactory);
		final DialogController mockController = mock(DialogController.class);
		// Act:
		manager.setDialogController(mockController);
		assertThat(manager.getDialogController(), is(mockController));
		verify(mockController).hasFactory();
		verify(mockController).setFactory(mockFactory);
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testDialogControllerWithFactorySpecified() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController mockController = mock(DialogController.class);
		when(mockController.hasFactory()).thenReturn(true);
		// Act:
		manager.setDialogController(mockController);
		assertThat(manager.getDialogController(), is(mockController));
		verify(mockController).hasFactory();
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testDialogFactoryWhenControllerHasNoFactory() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController mockController = mock(DialogController.class);
		manager.setDialogController(mockController);
		Mockito.clearInvocations(mockController);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		// Act:
		manager.setDialogFactory(mockFactory);
		assertThat(manager.getDialogFactory(), is(mockFactory));
		verifyZeroInteractions(mockFactory);
		verify(mockController).setFactory(mockFactory);
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testDialogFactoryWhenControllerHasFactory() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController mockController = mock(DialogController.class);
		when(mockController.hasFactory()).thenReturn(true);
		manager.setDialogController(mockController);
		Mockito.clearInvocations(mockController);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		// Act:
		manager.setDialogFactory(mockFactory);
		assertThat(manager.getDialogFactory(), is(mockFactory));
		verifyZeroInteractions(mockFactory);
		verify(mockController).setFactory(mockFactory);
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testAttachToPreferenceScreen() {
		// Arrange:
		final TestManager manager = new TestManager();
		final PreferenceScreen screen = createPreferenceScreen();
		final SettingDialogPreference mockPreference = mock(SettingDialogPreference.class);
		when(mockPreference.getDialogId()).thenReturn(1);
		when(mockPreference.handleDialogButtonClick(ArgumentMatchers.any(Dialog.class), anyInt())).thenReturn(true);
		screen.addPreference(mockPreference);
		// Act:
		manager.attachToPreferenceScreen(screen);
		// Assert:
		assertThat(manager.onAttachedToPreferenceInvoked, is(true));
		verify(mockPreference).getDialogId();
		verify(mockPreference).setOnClickListener(manager);
	}

	@Test(expected = IllegalStateException.class)
	public void testAttachToPreferenceScreenWhenAlreadyAttached() {
		// Arrange:
		final TestManager manager = new TestManager();
		final PreferenceScreen screen = createPreferenceScreen();
		final SettingDialogPreference mockPreference = mock(SettingDialogPreference.class);
		when(mockPreference.getDialogId()).thenReturn(1);
		when(mockPreference.handleDialogButtonClick(ArgumentMatchers.any(Dialog.class), anyInt())).thenReturn(true);
		screen.addPreference(mockPreference);
		manager.attachToPreferenceScreen(screen);
		// Act:
		manager.attachToPreferenceScreen(screen);
	}

	@Test public void testFindDialogPreference() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final PreferenceScreen screen = createPreferenceScreen();
		final SettingDialogPreference mockPreference = mock(SettingDialogPreference.class);
		when(mockPreference.getDialogId()).thenReturn(1);
		when(mockPreference.handleDialogButtonClick(ArgumentMatchers.any(Dialog.class), anyInt())).thenReturn(true);
		screen.addPreference(mockPreference);
		manager.attachToPreferenceScreen(screen);
		Mockito.clearInvocations(mockPreference);
		// Act:
		final SettingDialogPreference preference = manager.findDialogPreference(1);
		// Assert:
		assertThat(preference, is(notNullValue()));
		assertThat(preference, is(mockPreference));
	}

	@Test public void testFindDialogPreferenceWhenNotAttachedToScreen() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		// Act:
		final SettingDialogPreference preference = manager.findDialogPreference(1);
		// Assert:
		assertThat(preference, is(nullValue()));
	}

	@Test public void testDetachFromPreferenceScreen() {
		// Arrange:
		final TestManager manager = new TestManager();
		final PreferenceScreen screen = createPreferenceScreen();
		final SettingDialogPreference mockPreference = mock(SettingDialogPreference.class);
		when(mockPreference.getDialogId()).thenReturn(1);
		when(mockPreference.handleDialogButtonClick(ArgumentMatchers.any(Dialog.class), anyInt())).thenReturn(true);
		screen.addPreference(mockPreference);
		manager.attachToPreferenceScreen(screen);
		Mockito.clearInvocations(mockPreference);
		// Act:
		manager.detachFromPreferenceScreen(screen);
		// Assert:
		assertThat(manager.onDetachedFromPreferenceInvoked, is(true));
		verify(mockPreference).setOnClickListener(null);
		verifyNoMoreInteractions(mockPreference);
	}

	@Test public void testOnDialogPreferenceClick() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController controller = DialogController.create(createPreferenceFragment());
		final DialogFactory mockFactory = mock(DialogFactory.class);
		when(mockFactory.isDialogProvided(anyInt())).thenReturn(true);
		when(mockFactory.createDialog(anyInt(), ArgumentMatchers.any(DialogOptions.class))).thenReturn(mock(DialogFragment.class));
		controller.setFactory(mockFactory);
		manager.setDialogController(controller);
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setDialogId(1);
		// Act + Assert:
		assertThat(manager.onDialogPreferenceClick(preference), is(true));
		verify(mockFactory, times(2)).isDialogProvided(preference.getDialogId());
		verify(mockFactory).createDialog(eq(preference.getDialogId()), ArgumentMatchers.any(DialogOptions.class));
	}

	@Test public void testOnDialogPreferenceClickWhenDialogIsNotProvidedByFactory() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController mockController = mock(DialogController.class);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		when(mockFactory.isDialogProvided(anyInt())).thenReturn(false);
		when(mockController.getFactory()).thenReturn(mockFactory);
		manager.setDialogController(mockController);
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setDialogId(1);
		// Act + Assert:
		assertThat(manager.onDialogPreferenceClick(preference), is(false));
		verify(mockFactory).isDialogProvided(preference.getDialogId());
	}

	@Test public void testOnDialogPreferenceClickForPreferenceWithoutDialogId() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		// Act + Assert:
		assertThat(manager.onDialogPreferenceClick(preference), is(false));
	}

	@Test(expected = IllegalStateException.class)
	public void testOnDialogPreferenceClickWhenNoDialogControllerIsAttached() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setDialogId(1);
		// Act:
		manager.onDialogPreferenceClick(preference);
	}

	@Test(expected = IllegalStateException.class)
	public void testOnDialogPreferenceClickWhenNoDialogFactoryIsAttached() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		manager.setDialogController(DialogController.create(createPreferenceFragment()));
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setDialogId(1);
		// Act:
		manager.onDialogPreferenceClick(preference);
	}

	@Test public void testOnShowPreferenceDialog() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final DialogController controller = DialogController.create(createPreferenceFragment());
		final DialogFactory mockFactory = mock(DialogFactory.class);
		final DialogFragment mockDialogFragment = mock(DialogFragment.class);
		when(mockFactory.isDialogProvided(anyInt())).thenReturn(true);
		when(mockFactory.createDialog(anyInt(), ArgumentMatchers.any(DialogOptions.class))).thenReturn(mockDialogFragment);
		controller.setFactory(mockFactory);
		final SettingDialogPreference preference = new SettingDialogPreference(context());
		preference.setDialogId(1);
		// Act + Assert:
		assertThat(manager.onShowPreferenceDialog(controller, preference), is(true));
	}

	@Test public void testHandlePreferenceDialogButtonClick() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final PreferenceScreen screen = createPreferenceScreen();
		final SettingDialogPreference mockPreference = mock(SettingDialogPreference.class);
		when(mockPreference.getDialogId()).thenReturn(1);
		when(mockPreference.handleDialogButtonClick(ArgumentMatchers.any(Dialog.class), anyInt())).thenReturn(true);
		screen.addPreference(mockPreference);
		manager.attachToPreferenceScreen(screen);
		Mockito.clearInvocations(mockPreference);
		final Dialog mockDialog = mock(Dialog.class);
		when(mockDialog.getDialogId()).thenReturn(1);
		// Act + Assert:
		assertThat(manager.handlePreferenceDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getDialogId();
		verifyNoMoreInteractions(mockDialog);
		verify(mockPreference).handleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE);
		verifyNoMoreInteractions(mockPreference);
	}

	@Test public void testHandlePreferenceDialogButtonClickForPreferenceNotPresentedInScreen() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final PreferenceScreen screen = createPreferenceScreen();
		manager.attachToPreferenceScreen(screen);
		final Dialog mockDialog = mock(Dialog.class);
		// Act + Assert:
		assertThat(manager.handlePreferenceDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verify(mockDialog).getDialogId();
		verifyNoMoreInteractions(mockDialog);
	}

	@Test public void testHandlePreferenceDialogButtonClickWhenNotAttachedToScreen() {
		// Arrange:
		final SettingDialogPreferenceManager manager = new SettingDialogPreferenceManager();
		final Dialog mockDialog = mock(Dialog.class);
		// Act + Assert:
		assertThat(manager.handlePreferenceDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog);
	}

	public static class TestFragment extends Fragment {}

	public static class TestPreferenceFragment extends PreferenceFragmentCompat {

		/**
		 */
		@Override public void onCreatePreferences(@Nullable final Bundle savedInstanceState, @Nullable final String rootKey) {}
	}

	private final class TestManager extends SettingDialogPreferenceManager {

		boolean onAttachedToPreferenceInvoked;
		boolean onDetachedFromPreferenceInvoked;

		@Override protected void onAttachedToPreference(@NonNull SettingDialogPreference preference) {
			this.onAttachedToPreferenceInvoked = true;
		}

		@Override protected void onDetachedFromPreference(@NonNull SettingDialogPreference preference) {
			this.onDetachedFromPreferenceInvoked = true;
		}
	}
}