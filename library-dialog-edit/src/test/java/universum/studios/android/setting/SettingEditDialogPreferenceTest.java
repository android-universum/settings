/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.res.TypedArray;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.EditDialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingEditDialogPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingEditDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingEditDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context(), null, R.attr.settingEditDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getInput(), is(nullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context(), null, R.attr.settingEditDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getInput(), is(nullValue()));
	}

	@Test public void testOnCreateDialogOptions() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		preference.setTitle("Test title");
		// Act:
		final EditDialog.EditOptions options = preference.onCreateDialogOptions(context().getResources());
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is("Test title"));
		assertThat(options.content(), is(nullValue()));
		assertThat(options, is(not(preference.onCreateDialogOptions(context().getResources()))));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getString(0)).thenReturn("Test input");
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is("Test input"));
		verify(mockTypedArray).getString(0);
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		// Act + Assert:
		preference.onSetInitialValue(null);
		assertThat(preference.getInput(), is(nullValue()));
		preference.onSetInitialValue("Test input");
		assertThat(preference.getInput(), is("Test input"));
	}

	@Test public void testInput() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		// Act + Assert:
		preference.setInput("Test input");
		preference.setInput("Test input");
		assertThat(preference.getInput(), is("Test input"));
		preference.setInput("New input");
		assertThat(preference.getInput(), is("New input"));
	}

	@Test public void testOnBindView() throws Exception {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		preference.setInput("Test input");
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockSummaryView).setText("Test input");
		verify(mockSummaryView).setVisibility(View.VISIBLE);
	}

	@Test public void testOnGetSummaryText() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		preference.setSummary("Test summary.");
		// Act + Assert:
		assertThat(preference.onGetSummaryText(), is("Test summary."));
		preference.setInput("Test input");
		assertThat(preference.onGetSummaryText(), is("Test input"));
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		// Act + Assert:
		assertThat(preference.getDialogOptions().content(), is(nullValue()));
		preference.setInput("Test input");
		assertThat(preference.getDialogOptions().content(), is("Test input"));
	}

	@Test public void testOnHandleDialogButtonClickPositive() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final EditDialog mockDialog = mock(EditDialog.class);
		when(mockDialog.getInput()).thenReturn("Test input");
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getInput();
		verifyNoMoreInteractions(mockDialog);
		verify(mockChangeListener).onPreferenceChange(preference, "Test input");
		verifyNoMoreInteractions(mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickNegative() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final EditDialog mockDialog = mock(EditDialog.class);
		when(mockDialog.getInput()).thenReturn("Test input");
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_NEGATIVE), is(true));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickForNotEditDialog() {
		// Arrange:
		final SettingEditDialogPreference preference = new SettingEditDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final Dialog mockDialog = mock(SimpleDialog.class);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}
}