/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.EditDialog;
import universum.studios.android.dialog.view.InputConfig;

/**
 * A {@link SettingDialogPreference} implementation that may be used to allow to a user to specify
 * its preferred input text for a specific setting preference.
 * <p>
 * This preference implementation by default displays the preferred input text as its summary text
 * if it is specified, or the standard summary text if there is no input text persisted. The preferred
 * input text may be specified via {@link #setInput(String)} and obtained via {@link #getInput()}.
 * <p>
 * When {@link #handleDialogButtonClick(Dialog, int)} is called, this preference implementation
 * handles only {@link EditDialog} type of dialog. If its {@link Dialog#BUTTON_POSITIVE} button has
 * been clicked, the input provided via {@link EditDialog#getInput()} is set as input for this
 * preference via {@link #setInput(String)}.
 *
 * <h3>Default value</h3>
 * Default value for this preference is parsed as {@link String}. See {@link TypedArray#getString(int)}.
 *
 * <h3>Xml attributes</h3>
 * See {@link SettingDialogPreference},
 * {@link R.styleable#SettingEditDialogPreference SettingEditDialogPreference Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingEditDialogPreferenceStyle settingEditDialogPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SettingEditDialogPreference extends SettingDialogPreference<EditDialog.EditOptions> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingEditDialogPreference";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Boolean flag indicating whether the input value for this preference has been set or not.
	 * This flag is used to handle case when the same value is being specified for this preference,
	 * but for the first time, to properly refresh view of this preference and notify listeners about
	 * the change.
	 */
	private boolean inputSet;

	/**
	 * Current input value specified for this preference. This may be either value specified by the
	 * user, default value or persisted value.
	 */
	private String input;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingEditDialogPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingEditDialogPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingEditDialogPreference(Context, AttributeSet, int)} with
	 * {@link R.attr#settingEditDialogPreferenceStyle} as attribute for default style.
	 */
	public SettingEditDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingEditDialogPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingEditDialogPreference(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public SettingEditDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of SettingEditDialogPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingEditDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override protected void onConfigureDialogOptions(
			@NonNull final EditDialog.EditOptions options,
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super.onConfigureDialogOptions(options, context, attrs, defStyleAttr, defStyleRes);
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingEditDialogPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingEditDialogPreference_dialogHint) {
				options.hint(attributes.getText(attrIndex));
			} else if (attrIndex == R.styleable.SettingEditDialogPreference_dialogInputStyle) {
				options.inputConfig(InputConfig.fromStyle(context, attributes.getResourceId(attrIndex, 0)));
			} else if (attrIndex == R.styleable.SettingEditDialogPreference_dialogShowSoftKeyboard) {
				options.showSoftKeyboard(attributes.getBoolean(attrIndex, options.shouldShowSoftKeyboard()));
			}
		}
		attributes.recycle();
	}

	/**
	 */
	@Override @NonNull protected EditDialog.EditOptions onCreateDialogOptions(@NonNull final Resources resources) {
		return new EditDialog.EditOptions(resources).title(getTitle());
	}

	/**
	 */
	@Override protected Object onGetDefaultValue(@NonNull final TypedArray typedArray, final int index) {
		return typedArray.getString(index);
	}

	/**
	 */
	@Override protected void onSetInitialValue(@Nullable final Object defaultValue) {
		setInput(getPersistedString((String) defaultValue));
	}

	/**
	 * Sets a preferred input value for this preference.
	 * <p>
	 * If value of this preference changes, it is persisted and the change listener is notified
	 * about the change.
	 *
	 * @param input The preferred input to be persisted.
	 *
	 * @see #getInput()
	 */
	public void setInput(@Nullable final String input) {
		final boolean changed = !TextUtils.equals(this.input, input);
		if (changed || !inputSet) {
			this.input = input;
			this.inputSet = true;
			persistString(this.input);
			if (changed) {
				notifyChanged();
			}
		}
	}

	/**
	 * Returns the preferred input value of this preference.
	 *
	 * @return Input either specified by the user, as default value or the persisted one.
	 *
	 * @see #setInput(String)
	 * @see #getDialogOptions()
	 */
	@Nullable public String getInput() {
		return input;
	}

	/**
	 */
	@Override public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);
		synchronizeSummaryView(holder);
	}

	/**
	 */
	@Override @Nullable protected CharSequence onGetSummaryText() {
		return inputSet ? input : super.onGetSummaryText();
	}

	/**
	 * Dialog options of this preference with the preferred input specified as
	 * {@link EditDialog.EditOptions#content(CharSequence)}, if it is set.
	 *
	 * @see #getInput()
	 */
	@Override @NonNull public EditDialog.EditOptions getDialogOptions() {
		final EditDialog.EditOptions options = super.getDialogOptions();
		if (inputSet) {
			options.content(input);
		}
		return options;
	}

	/**
	 */
	@Override protected boolean onHandleDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
		if (dialog instanceof EditDialog) {
			switch (button) {
				case Dialog.BUTTON_POSITIVE:
					final String newInput = ((EditDialog) dialog).getInput().toString();
					if (callChangeListener(newInput)) {
						setInput(newInput);
					}
					return true;
				default:
					return true;
			}
		}
		return super.onHandleDialogButtonClick(dialog, button);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}