Settings-Dialog-Edit
===============

This module contains dialog preference implementation that allows to set a desired **text** setting
value that may be specified via **edit text** widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-dialog-edit:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core),
[settings-dialog-base](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base),
[universum.studios.android:dialogs](https://bitbucket.org/android-universum/dialogs)

Below are listed some of **primary elements** that are available in this module:

- [SettingEditDialogPreference](https://bitbucket.org/android-universum/settings/src/main/library-dialog-edit/src/main/java/universum/studios/android/setting/SettingEditDialogPreference.java)
