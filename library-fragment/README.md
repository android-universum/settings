Settings-Fragment
===============

This module contains implementation of `PreferenceFragment` that may be used as base class for
setting fragments in an **Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-fragment:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

Below are listed some of **primary elements** that are available in this module:

- [SettingsBaseFragment](https://bitbucket.org/android-universum/settings/src/main/library-fragment/src/main/java/universum/studios/android/setting/SettingsBaseFragment.java)
