/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import org.junit.Ignore;
import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SettingsBaseFragmentTest extends AndroidTestCase {

	private TestFragment createFragment() {
		final FragmentActivity activity = Robolectric.buildActivity(TestPreferenceActivity.class).create().get();
		final TestFragment fragment = new TestFragment();
		activity.getSupportFragmentManager().beginTransaction().add(fragment, null).commitAllowingStateLoss();
		return fragment;
	}

	@Test public void testInstantiation() {
		// Act:
		createFragment();
	}

	@Test public void testCheckSelfPermission() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act + Assert:
		assertThat(fragment.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE), is(PackageManager.PERMISSION_DENIED));
	}

	@Config(sdk = Build.VERSION_CODES.KITKAT)
	@Test public void testShouldShowRequestPermissionRationaleAtKitKatApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act + Assert:
		assertThat(fragment.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE), is(false));
	}

	@Config(sdk = Build.VERSION_CODES.M)
	@Test public void testShouldShowRequestPermissionRationaleAtMarshmallowApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act + Assert:
		assertThat(fragment.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE), is(false));
	}

	@Config(sdk = Build.VERSION_CODES.KITKAT)
	@Test public void testSupportRequestPermissionsAtKitKatApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act:
		fragment.supportRequestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
	}

	@Config(sdk = Build.VERSION_CODES.M)
	@Test public void testSupportRequestPermissionsAtMarshmallowApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act:
		fragment.supportRequestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
	}

	@Config(sdk = Build.VERSION_CODES.KITKAT)
	@Test public void testOnRequestPermissionsResultAtKitKatApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act:
		fragment.onRequestPermissionsResult(1, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, new int[]{PackageManager.PERMISSION_DENIED});
	}

	@Config(sdk = Build.VERSION_CODES.M)
	@Test public void testOnRequestPermissionsResultAtMarshmallowApiLevel() {
		// Arrange:
		final SettingsBaseFragment fragment = createFragment();
		// Act:
		fragment.onRequestPermissionsResult(1, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, new int[]{PackageManager.PERMISSION_DENIED});
	}

	@Ignore("Due to: Resources$NotFoundException")
	@Test public void testAddPreferencesFromResource() {
		// Arrange:
		final TestFragment fragment = createFragment();
		// Act:
		fragment.addPreferencesFromResource(R.xml.settings_empty);
		// Assert:
		assertThat(fragment.onPreferencesAddedInvoked, is(true));
	}

	@Ignore("Due to: Resources$NotFoundException")
	@Test public void testRequestBindPreferences() {
		// Arrange:
		final TestFragment fragment = createFragment();
		fragment.addPreferencesFromResource(R.xml.settings_empty);
		// Act:
		fragment.requestBindPreferences();
		// Assert:
		assertThat(fragment.onBindPreferencesInvoked, is(true));
	}

	@Test public void testRequestBindPreferencesWhenPreferencesAreNotAdded() {
		// Arrange:
		final TestFragment fragment = createFragment();
		// Act:
		fragment.requestBindPreferences();
		// Assert:
		assertThat(fragment.onBindPreferencesInvoked, is(false));
	}

	public static final class TestPreferenceActivity extends TestActivity {

		@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
			setTheme(R.style.Setting_Theme);
			super.onCreate(savedInstanceState);
		}
	}

	public static final class TestFragment extends SettingsBaseFragment {

		boolean onPreferencesAddedInvoked;
		boolean onBindPreferencesInvoked;

		@Override public void onCreatePreferences(@Nullable final Bundle savedInstanceState, @Nullable final String rootKey) {}

		@Override protected void onPreferencesAdded() {
			super.onPreferencesAdded();
			this.onPreferencesAddedInvoked = true;
		}

		@Override protected void onBindPreferences() {
			super.onBindPreferences();
			this.onBindPreferencesInvoked = true;
		}
	}
}