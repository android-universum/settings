/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceFragment;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.XmlRes;
import androidx.core.app.ActivityCompat;
import androidx.preference.PreferenceFragmentCompat;

/**
 * A {@link PreferenceFragment} implementation which inflates its layout from a style specified in
 * the current theme.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("RedundantCast")
public abstract class SettingsBaseFragment extends PreferenceFragmentCompat {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingsBaseFragment";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Boolean flag indicating whether there is a pending request for preferences binding registered
	 * or not.
	 *
	 * @see #requestBindPreferences()
	 */
	private boolean pendingBindPreferencesRequest;

	/**
	 * Boolean flag indicating whether the preferences for this fragment has been added via
	 * {@link #addPreferencesFromResource(int)}.
	 */
	private boolean preferencesAdded;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Delegated call to {@link ActivityCompat#checkSelfPermission(Context, String)}.
	 *
	 * @param permission The desired permission for which to perform check.
	 * @return {@link android.content.pm.PackageManager#PERMISSION_GRANTED} if you have the
	 * permission, or {@link android.content.pm.PackageManager#PERMISSION_DENIED} if not.
	 */
	@CheckResult protected int checkSelfPermission(@NonNull final String permission) {
		return ActivityCompat.checkSelfPermission(requireContext(), permission);
	}

	/**
	 */
	@Override @CheckResult public boolean shouldShowRequestPermissionRationale(@NonNull final String permission) {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && super.shouldShowRequestPermissionRationale(permission);
	}

	/**
	 * Invokes {@link #requestPermissions(String[], int)} on Android versions above {@link Build.VERSION_CODES#M Marshmallow}
	 * (including).
	 * <p>
	 * Calling this method on Android versions before <b>MARSHMALLOW</b> will be ignored.
	 *
	 * @param permissions The desired set of permissions to request.
	 * @param requestCode Code to identify this request in {@link #onRequestPermissionsResult(int, String[], int[])}.
	 */
	protected void supportRequestPermissions(@NonNull final String[] permissions, final int requestCode) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			requestPermissions(permissions, requestCode);
	}

	/**
	 */
	@Override public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	/**
	 * Adds a preferences from the specified <var>preferencesResId</var> Xml resource and registers
	 * this fragment as {@link android.preference.Preference.OnPreferenceChangeListener} on the current
	 * preference screen and calls {@link #onPreferencesAdded()}.
	 */
	@Override public void addPreferencesFromResource(@XmlRes final int preferencesResId) {
		super.addPreferencesFromResource(preferencesResId);
		this.handlePreferencesAdded();
	}

	/**
	 * Handles call to one of {@link #addPreferencesFromResource(int)} methods.
	 */
	private void handlePreferencesAdded() {
		this.preferencesAdded = true;
		onPreferencesAdded();
	}

	/**
	 * Called immediately after {@link #addPreferencesFromResource(int)} or when preferences for this
	 * fragment has been added into the associated preference screen.
	 * <p>
	 * Default implementation invokes {@link #onBindPreferences()} if binding of preferences has been
	 * requested before actually adding them.
	 *
	 * @see #getPreferenceScreen()
	 */
	protected void onPreferencesAdded() {
		if (pendingBindPreferencesRequest) {
			this.pendingBindPreferencesRequest = false;
			onBindPreferences();
		}
	}

	/**
	 * Requests binding of preferences via {@link #onBindPreferences()}.
	 * <p>
	 * If preferences for this fragment has not been added yet via {@link #addPreferencesFromResource(int)},
	 * a pending request to bind preferences is registered and binding will be performed when
	 * {@link #onPreferencesAdded()} is called, otherwise call to this method immediately invokes
	 * \{@link #onBindPreferences()}.
	 */
	protected void requestBindPreferences() {
		if (preferencesAdded) onBindPreferences();
		else this.pendingBindPreferencesRequest = true;
	}

	/**
	 * Called immediately after {@link #requestBindPreferences()} if preferences for this fragment
	 * has been already added via {@link #addPreferencesFromResource(int)} methods.
	 */
	protected void onBindPreferences() {
		// Inheritance hierarchies may perform here binding of the preferences presented in context
		// of this fragment.
	}

	/*
	 * Inner classes ===============================================================================
	 */
}