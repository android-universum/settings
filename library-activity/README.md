Settings-Activity
===============

This module contains implementation of `PreferenceActivity` that may be used as base class for
setting activities in an **Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-activity:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SettingsBaseActivity](https://bitbucket.org/android-universum/settings/src/main/library-activity/src/main/java/universum/studios/android/setting/SettingBaseActivity.java)
- [SettingHeadersAdapter](https://bitbucket.org/android-universum/settings/src/main/library-activity/src/main/java/universum/studios/android/setting/SettingHeadersAdapter.java)
