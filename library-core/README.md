Settings-Core
===============

This module contains core elements for this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SettingPreference](https://bitbucket.org/android-universum/settings/src/main/library-core/src/main/java/universum/studios/android/setting/SettingPreference.java)
- [SettingPreferenceCategory](https://bitbucket.org/android-universum/settings/src/main/library-core/src/main/java/universum/studios/android/setting/SettingPreferenceCategory.java)
