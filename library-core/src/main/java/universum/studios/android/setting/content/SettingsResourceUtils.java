/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting.content;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

/**
 * <b>Utils providing compatibility support for access to Android resources content.</b>
 *
 * @author Martin Albedinsky
 */
public final class SettingsResourceUtils {

    /**
     * Boolean flag indicating whether we can use resources access in a way appropriate for
     * {@link Build.VERSION_CODES#LOLLIPOP} Android version.
     */
    private static final boolean ACCESS_LOLLIPOP = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

	/**
	 */
	private SettingsResourceUtils() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

    /**
     * Obtains vector drawable with the specified <var>resId</var> using the given <var>resources</var>.
     * <p>
     * This utility method will obtain the requested vector drawable in a way that is appropriate
     * for the current Android version.
     *
     * @param resources The resources that should be used to obtain the vector drawable.
     * @param resId     Resource id of the desired vector drawable to obtain.
     * @param theme     Theme that will be used to resolve theme attributes for the requested drawable
     *                  on {@link Build.VERSION_CODES#LOLLIPOP} and above Android versions.
     * @return Instance of the requested vector drawable or {@code null} if the specified resource
     * id is {@code 0}.
     * @see #getDrawable(Resources, int, Resources.Theme)
     * @see VectorDrawableCompat#create(Resources, int, Resources.Theme)
     */
    @Nullable public static Drawable getVectorDrawable(
    		@NonNull final Resources resources,
		    @DrawableRes final int resId,
		    @Nullable final Resources.Theme theme
    ) throws Resources.NotFoundException {
        if (resId == 0) {
	        return null;
        }
        return ACCESS_LOLLIPOP ? getDrawable(resources, resId, theme) : VectorDrawableCompat.create(resources, resId, theme);
    }

    /**
     * Obtains drawable with the specified <var>resId</var> using the given <var>resources</var>.
     * <p>
     * This utility method will obtain the requested drawable in a way that is appropriate for the
     * current Android version.
     *
     * @param resources The resources that should be used to obtain the drawable.
     * @param resId     Resource id of the desired drawable to obtain.
     * @param theme     Theme that will be used to resolve theme attributes for the requested drawable
     *                  on {@link Build.VERSION_CODES#LOLLIPOP} and above Android versions.
     * @return Instance of the requested drawable or {@code null} if the specified resource id is {@code 0}.
     * @see Resources#getDrawable(int, Resources.Theme)
     * @see Resources#getDrawable(int)
     */
    @SuppressWarnings({"NewApi", "deprecation"})
    @Nullable public static Drawable getDrawable(
    		@NonNull final Resources resources,
		    @DrawableRes final int resId,
		    @Nullable final Resources.Theme theme
    ) throws Resources.NotFoundException {
        if (resId == 0) {
	        return null;
        }
        return ACCESS_LOLLIPOP ? resources.getDrawable(resId, theme) : resources.getDrawable(resId);
    }
}