/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.content.SettingsResourceUtils;

/**
 * PreferenceDecorator is used by extended versions of preferences to provide extended API functionality
 * for them.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class PreferenceDecorator {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "PreferenceDecorator";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Preference for which is this decorator created.
	 */
	private final Preference preference;

	/**
	 * Default value resolved from Xml attributes for the associated preference.
	 */
	@VisibleForTesting Object defaultValue;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of PreferenceDecorator for the given <var>preference</var>.
	 *
	 * @param preference The preference for which to create new decorator.
	 */
	PreferenceDecorator(final Preference preference) {
		this.preference = preference;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * This should be called from the attached preference during its initialization.
	 *
	 * @param context      The context that can be used to access resource values.
	 * @param attrs        Set of attributes passed to the preference's constructor.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     the attached preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the attached preference.
	 */
	void processAttributes(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingPreference_android_defaultValue) {
				this.defaultValue = onGetDefaultValue(attributes, attrIndex);
			} else if (attrIndex == R.styleable.SettingPreference_settingVectorIcon) {
				setVectorIcon(attributes.getResourceId(attrIndex, 0));
			}
		}
		attributes.recycle();
	}

	/**
	 * Invoked to obtain default value for the associated preference from the given <var>attributes</var>
	 * array.
	 *
	 * @param attributes The attributes array from which to obtain default value.
	 * @param index      Index at which should be the value obtained.
	 * @return The default value specific for the preference or {@code null} if the preference does
	 * not have default value attached.
	 */
	@Nullable abstract Object onGetDefaultValue(@NonNull TypedArray attributes, int index);

	/**
	 * Handles change in the key of the associated preference.
	 * <p>
	 * This implementation dispatches update of initial value to the associated preference.
	 */
	void handleKeyChange() {
		updateInitialValue();
	}

	/**
	 * Dispatches request for update of initial value to the associated preference via {@link #onUpdateInitialValue(Object)}.
	 */
	private void updateInitialValue() {
		onUpdateInitialValue(defaultValue);
	}

	/**
	 * Checks whether the value of the associated preference should be persisted or not.
	 *
	 * @return {@code True} if value should be persisted, {@code false} otherwise.
	 */
	private boolean shouldPersist() {
		return preference.getPreferenceManager() != null && preference.isPersistent() && preference.hasKey();
	}

	/**
	 * Invoked to update initial value of the associated preference.
	 *
	 * @param defaultValue The default value to use as initial one if none is persisted at this time.
	 */
	abstract void onUpdateInitialValue(@Nullable Object defaultValue);

	/**
	 * Returns the default value resolved from Xml attributes for the associated preference.
	 *
	 * @return Default value or {@code null} if no value has been resolved.
	 */
	@Nullable final Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets a vector icon for the attached preference via {@link Preference#setIcon(Drawable)} where
	 * the vector drawable will be obtained via {@link SettingsResourceUtils#getVectorDrawable(Resources, int, Resources.Theme)}.
	 *
	 * @param resId Resource id of the desired vector icon to set. May be {@code 0} to clear the
	 *              current icon.
	 */
	void setVectorIcon(@DrawableRes final int resId) {
		final Context context = preference.getContext();
		preference.setIcon(SettingsResourceUtils.getVectorDrawable(context.getResources(), resId, context.getTheme()));
	}

	/**
	 * This should be called from the attached preference whenever its
	 * {@link Preference#onBindViewHolder(PreferenceViewHolder)} method is invoked.
	 *
	 * @param viewHolder The view holder bound by the preference where the decorator may perform
	 *                   additional modifications.
	 */
	void onBindViewHolder(@NonNull final PreferenceViewHolder viewHolder) {
		final View iconFrameView = viewHolder.findViewById(R.id.setting_icon_frame);
		if (iconFrameView != null) {
			iconFrameView.setVisibility(preference.getIcon() == null ? View.GONE : View.VISIBLE);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}