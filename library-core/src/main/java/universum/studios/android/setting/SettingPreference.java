/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

/**
 * Extended {@link Preference} that provides additional features supported by the <b>Settings</b>
 * library.
 *
 * <h3>Xml attributes</h3>
 * See {@link Preference}
 *
 * <h3>Default style attribute</h3>
 * {@link android.R.attr#preferenceStyle android:preferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SettingPreference extends Preference {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingPreference";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Decorator used to extend API of this setting preference by functionality otherwise not supported
	 * or not available due to current API level.
	 */
	private PreferenceDecorator decorator;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingPreference(Context, AttributeSet, int)} with
	 * {@link android.R.attr#preferenceStyle} as attribute for default style.
	 */
	public SettingPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, android.R.attr.preferenceStyle);
	}

	/**
	 * Same as {@link #SettingPreference(Context, AttributeSet, int, int)} with {@code 0} as default
	 * style.
	 */
	public SettingPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of SettingPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this setting preference to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this preference specific data from it that can be used to configure this new preference instance.
	 * The specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default
	 * data from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.ensureDecorator();
		this.decorator.processAttributes(context, attrs, defStyleAttr, defStyleRes);
	}

	/**
	 * Ensures that the decorator for this view is initialized.
	 */
	private void ensureDecorator() {
		if (decorator == null) this.decorator = new PreferenceDecorator(this) {

			/**
			 */
			@Override @Nullable Object onGetDefaultValue(@NonNull final TypedArray attributes, final int index) {
				return SettingPreference.this.onGetDefaultValue(attributes, index);
			}

			/**
			 */
			@Override void onUpdateInitialValue(@Nullable final Object defaultValue) {
				SettingPreference.this.onSetInitialValue(defaultValue);
			}
		};
	}

	/**
	 */
	@Override public void setKey(@NonNull final String key) {
		final boolean changed = !key.equals(getKey());
		super.setKey(key);
		if (changed) {
			this.decorator.handleKeyChange();
		}
	}

	/**
	 */
	@Override public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);
		this.ensureDecorator();
		this.decorator.onBindViewHolder(holder);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}