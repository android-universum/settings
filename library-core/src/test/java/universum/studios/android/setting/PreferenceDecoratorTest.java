/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class PreferenceDecoratorTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final Preference preference = new Preference(context());
		// Act:
		new TestDecorator(preference);
	}

	@Test public void testUpdateInitialValue() {
		// Arrange:
		final Preference mockPreference = createMockPersistablePreference(true);
		final TestDecorator decorator = new TestDecorator(mockPreference);
		// Act:
		decorator.handleKeyChange();
		// Assert:
		assertThat(decorator.updateInitialValueInvoked, is(true));
		assertThat(decorator.updateInitialValueDefaultValue, is(nullValue()));
	}

	@Test public void testOnBindViewForPreferenceWithIcon() {
		// Arrange:
		final PreferenceViewHolder mockViewHolder = mock(PreferenceViewHolder.class);
		final View mockIconFrame = mock(View.class);
		when(mockViewHolder.findViewById(R.id.setting_icon_frame)).thenReturn(mockIconFrame);
		final Preference mockPreference = mock(Preference.class);
		when(mockPreference.getIcon()).thenReturn(new ColorDrawable(Color.BLACK));
		final TestDecorator decorator = new TestDecorator(mockPreference);
		// Act:
		decorator.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(R.id.setting_icon_frame);
		verifyNoMoreInteractions(mockViewHolder);
		verify(mockIconFrame).setVisibility(View.VISIBLE);
		verifyNoMoreInteractions(mockIconFrame);
		verify(mockPreference).getIcon();
		verifyNoMoreInteractions(mockPreference);
	}

	@Test public void testOnBindViewForPreferenceWithoutIcon() throws Exception {
		// Arrange:
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final View mockIconFrame = mock(View.class);
		when(mockViewHolder.findViewById(R.id.setting_icon_frame)).thenReturn(mockIconFrame);
		final Preference mockPreference = mock(Preference.class);
		when(mockPreference.getIcon()).thenReturn(null);
		final TestDecorator decorator = new TestDecorator(mockPreference);
		// Act:
		decorator.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(R.id.setting_icon_frame);
		verifyNoMoreInteractions(mockViewHolder);
		verify(mockIconFrame).setVisibility(View.GONE);
		verifyNoMoreInteractions(mockIconFrame);
		verify(mockPreference).getIcon();
		verifyNoMoreInteractions(mockPreference);
	}

	@Test public void testOnBindViewWithoutIconFrameInHierarchy() throws Exception {
		// Arrange:
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		when(mockViewHolder.findViewById(R.id.setting_icon_frame)).thenReturn(null);
		final Preference mockPreference = mock(Preference.class);
		final TestDecorator decorator = new TestDecorator(mockPreference);
		// Act:
		decorator.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(R.id.setting_icon_frame);
		verifyNoMoreInteractions(mockViewHolder);
		verifyZeroInteractions(mockPreference);
	}

	private static Preference createMockPersistablePreference(final boolean persistable) {
		final Preference mockPreference = mock(Preference.class);
		when(mockPreference.hasKey()).thenReturn(true);
		when(mockPreference.isPersistent()).thenReturn(persistable);
		when(mockPreference.getPreferenceManager()).thenReturn(mock(PreferenceManager.class));
		when(mockPreference.getSharedPreferences()).thenReturn(mock(SharedPreferences.class));
		return mockPreference;
	}

	private static final class TestDecorator extends PreferenceDecorator {

		boolean updateInitialValueInvoked;
		Object updateInitialValueDefaultValue;

		TestDecorator(final Preference preference) {
			super(preference);
		}

		@Override @Nullable Object onGetDefaultValue(@NonNull final TypedArray attributes, final int index) {
			return null;
		}

		@Override void onUpdateInitialValue(@Nullable final Object defaultValue) {
			this.updateInitialValueInvoked = true;
			this.updateInitialValueDefaultValue = defaultValue;
		}
	}
}