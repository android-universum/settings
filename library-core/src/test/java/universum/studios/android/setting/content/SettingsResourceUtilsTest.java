/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting.content;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public class SettingsResourceUtilsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		SettingsResourceUtils.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		final Constructor constructor = SettingsResourceUtils.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		constructor.newInstance();
	}

	@Test public void testGetVectorDrawable() {
		// Act + Assert:
		assertThat(SettingsResourceUtils.getVectorDrawable(context().getResources(), android.R.drawable.ic_delete, null), is(notNullValue()));
	}

	@Test public void testGetVectorDrawableForUnspecifiedResourceId() {
		// Act + Assert:
		assertThat(SettingsResourceUtils.getVectorDrawable(context().getResources(), 0, null), is(nullValue()));
	}

	@Test public void testGetDrawable() {
		// Act + Assert:
		assertThat(SettingsResourceUtils.getDrawable(context().getResources(), android.R.drawable.ic_delete, null), is(notNullValue()));
	}

	@Test public void testGetDrawableForUnspecifiedResourceId() {
		// Act + Assert:
		assertThat(SettingsResourceUtils.getDrawable(context().getResources(), 0, null), is(nullValue()));
	}
}