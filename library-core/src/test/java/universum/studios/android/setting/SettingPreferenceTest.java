/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Build;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Albedinsky
 */
public final class SettingPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		new SettingPreference(context(), null, android.R.attr.preferenceStyle);
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		new SettingPreference(context(), null, android.R.attr.preferenceStyle, 0);
	}

	@Test public void testSetKey() {
		// Arrange:
		final SettingPreference preference = new SettingPreference(context());
		// Act + Assert:
		preference.setKey("KEY.Test.1");
		assertThat(preference.getKey(), is("KEY.Test.1"));
		preference.setKey("KEY.Test.2");
		preference.setKey("KEY.Test.2");
		assertThat(preference.getKey(), is("KEY.Test.2"));
	}

	@Test public void testOnBindViewHolder() throws Exception {
		// Arrange:
		final SettingPreference preference = new SettingPreference(context());
		final PreferenceViewHolder mockView = TestPreferenceViewHolders.createMockHolder(context());
		// Act:
		preference.onBindViewHolder(mockView);
		// Assert:
		verify(mockView).findViewById(android.R.id.icon_frame);
	}
}