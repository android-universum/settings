/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.dialog.DatePickerDialog;
import universum.studios.android.dialog.Dialog;

/**
 * A {@link SettingDateTimeDialogPreference} implementation that may be used to allow to a user to pick
 * its preferred date for a specific setting preference.
 * <p>
 * This preference implementation by default displays the preferred date formatted by format specified
 * via {@link #setFormat(SimpleDateFormat)} as summary text. The default format is {@code 'MMM dd, yyyy'}.
 * If no date is specified, the standard summary text is displayed. The preferred date may be specified
 * via {@link #setDate(long)} and obtained via {@link #getDate()} or {@link #getDateInMillis()}.
 * <p>
 * When {@link #handleDialogButtonClick(Dialog, int)} is called, this preference implementation
 * handles only {@link DatePickerDialog} type of dialog. If its {@link Dialog#BUTTON_POSITIVE} button
 * has been clicked, the date provided via {@link DatePickerDialog#getDate()} is set as date for
 * this preference via {@link #setDate(long)}.
 *
 * <h3>Default value</h3>
 * Default value for this preference is parsed as {@link String} into <b>milliseconds</b> value
 * using {@link DatePickerDialog.DateParser#parse(String)}. See {@link TypedArray#getString(int)}.
 *
 * <h3>Xml attributes</h3>
 * See {@link SettingDateTimeDialogPreference},
 * {@link R.styleable#SettingDateDialogPreference SettingDateDialogPreference Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingDateDialogPreferenceStyle settingDateDialogPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see SettingTimeDialogPreference
 */
public final class SettingDateDialogPreference extends SettingDateTimeDialogPreference<DatePickerDialog.DateOptions> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingDateDialogPreference";

	/**
	 * Default pattern for the date format.
	 */
	@VisibleForTesting static final String DEFAULT_FORMAT_PATTERN = "MMM dd, yyyy";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingDateDialogPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingDateDialogPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingDateDialogPreference(Context, AttributeSet, int)} with
	 * {@link R.attr#settingDateDialogPreferenceStyle} as attribute for default style.
	 */
	public SettingDateDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingDateDialogPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingDateDialogPreference(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public SettingDateDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of SettingInputPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingDateDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Parses the specified string data as date via {@link DatePickerDialog.DateParser#parse(String)}.
	 *
	 * @param dateString The desired data to parse into date.
	 * @return Parsed date in milliseconds or {@code null} if parsing failed.
	 */
	private static Long parseDate(final String dateString) {
		return TextUtils.isEmpty(dateString) ? null : DatePickerDialog.DateParser.parse(dateString);
	}

	/**
	 */
	@Override @NonNull protected DatePickerDialog.DateOptions onCreateDialogOptions(@NonNull final Resources resources) {
		return new DatePickerDialog.DateOptions(resources);
	}

	/**
	 */
	@Override protected void onConfigureDialogOptions(
			@NonNull final DatePickerDialog.DateOptions options,
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super.onConfigureDialogOptions(options, context, attrs, defStyleAttr, defStyleRes);
		String formatPattern = DEFAULT_FORMAT_PATTERN;
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingDateDialogPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingDateDialogPreference_settingDateFormat) {
				final String dateFormat = attributes.getString(attrIndex);
				formatPattern = TextUtils.isEmpty(dateFormat) ? formatPattern : dateFormat;
			} else if (attrIndex == R.styleable.SettingDateDialogPreference_dialogDate) {
				final Long date = parseDate(attributes.getString(attrIndex));
				if (date != null) options.date(date);
			} else if (attrIndex == R.styleable.SettingDateDialogPreference_dialogDateMin) {
				final Long date = parseDate(attributes.getString(attrIndex));
				if (date != null) options.minDate(date);
			} else if (attrIndex == R.styleable.SettingDateDialogPreference_dialogDateMax) {
				final Long date = parseDate(attributes.getString(attrIndex));
				if (date != null) options.maxDate(date);
			}
		}
		attributes.recycle();
		setFormat(new SimpleDateFormat(formatPattern, Locale.getDefault()));
	}

	/**
	 */
	@Override long onParseDefaultValue(@NonNull final Object defaultValue) {
		final Long date = parseDate((String) defaultValue);
		return date == null ? 0 : date;
	}

	/**
	 * Same as {@link #setDate(long)} with milliseconds value of the given <var>date</var>.
	 *
	 * @param date The desired date with milliseconds value. May be {@code null} which corresponds
	 *             to {@code 0}.
	 *
	 * @see #getDate()
	 */
	public void setDate(@Nullable final Date date) {
		setDate(date == null ? 0 : date.getTime());
	}

	/**
	 * Sets a preferred date value for this preference.
	 * <p>
	 * If value of this preference changes, it is persisted and the change listener is notified
	 * about the change.
	 *
	 * @param dateInMillis The preferred date in milliseconds to be persisted.
	 *
	 * @see #getDateInMillis()
	 */
	public void setDate(final long dateInMillis) {
		setMilliseconds(dateInMillis);
	}

	/**
	 * Like {@link #getDateInMillis()}, but this method may be used to check whether the date value
	 * has been specified or not, as this method will return {@code null} Date object if there is
	 * no date value specified.
	 *
	 * @return Date as date object. May be {@code null} if no date value has been specified yet.
	 *
	 * @see #setDate(Date)
	 * @see #getDialogOptions()
	 */
	@Nullable public Date getDate() {
		return areMillisecondsSet() ? new Date(getDateInMillis()) : null;
	}

	/**
	 * Returns the preferred date value of this preference.
	 *
	 * @return Date in milliseconds either specified by the user, as default value or the persisted one.
	 *
	 * @see #setDate(long)
	 * @see #getDialogOptions()
	 */
	public long getDateInMillis() {
		return getMilliseconds();
	}

	/**
	 * Dialog options of this preference with the preferred date specified as
	 * {@link DatePickerDialog.DateOptions#date(long)}, if it is set.
	 *
	 * @see #getDateInMillis()
	 */
	@Override @NonNull public DatePickerDialog.DateOptions getDialogOptions() {
		final DatePickerDialog.DateOptions options = super.getDialogOptions();
		if (areMillisecondsSet()) {
			options.date(getDateInMillis());
		}
		return options;
	}

	/**
	 */
	@Override protected boolean onHandleDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
		if (dialog instanceof DatePickerDialog) {
			switch (button) {
				case Dialog.BUTTON_POSITIVE:
					final long newDate = ((DatePickerDialog) dialog).getDate();
					if (newDate != getDateInMillis() && callChangeListener(newDate)) {
						setDate(newDate);
					}
					return true;
				default:
					return true;
			}
		}
		return super.onHandleDialogButtonClick(dialog, button);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}