/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.TimePickerDialog;

/**
 * A {@link SettingDateTimeDialogPreference} implementation that may be used to allow to a user to pick
 * its preferred time for a specific setting preference.
 * <p>
 * This preference implementation by default displays the preferred time formatted by format specified
 * via {@link #setFormat(SimpleDateFormat)} as summary text. The default format is {@code 'hh:mm a'}.
 * If no time is specified, the standard summary text is displayed. The preferred time may be specified
 * via {@link #setTime(Date)} and obtained via {@link #getTime()} or {@link #getTimeInMillis()}.
 * <p>
 * When {@link #handleDialogButtonClick(Dialog, int)} is called, this preference implementation
 * handles only {@link TimePickerDialog} type of dialog. If its {@link Dialog#BUTTON_POSITIVE} button
 * has been clicked, the time provided via {@link TimePickerDialog#getTime()} is set as time for
 * this preference via {@link #setTime(long)}.
 *
 * <h3>Default value</h3>
 * Default value for this preference is parsed as {@link String} into <b>milliseconds</b> value
 * using {@link TimePickerDialog.TimeParser#parse(String)}. See {@link TypedArray#getString(int)}.
 *
 * <h3>Xml attributes</h3>
 * See {@link SettingDateTimeDialogPreference},
 * {@link R.styleable#SettingTimeDialogPreference SettingTimeDialogPreference Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingTimeDialogPreferenceStyle settingTimeDialogPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see SettingDateDialogPreference
 */
public class SettingTimeDialogPreference extends SettingDateTimeDialogPreference<TimePickerDialog.TimeOptions> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingTimeDialogPreference";

	/**
	 * Default pattern for the time format.
	 */
	@VisibleForTesting static final String DEFAULT_FORMAT_PATTERN = "hh:mm a";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingTimeDialogPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingTimeDialogPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingTimeDialogPreference(Context, AttributeSet, int)} with
	 * {@link R.attr#settingTimeDialogPreferenceStyle} as attribute for default style.
	 */
	public SettingTimeDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingTimeDialogPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingTimeDialogPreference(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public SettingTimeDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of SettingInputPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingTimeDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Parses the specified string data as time via {@link TimePickerDialog.TimeParser#parse(String)}.
	 *
	 * @param timeString The desired data to parse into time.
	 * @return Parsed time in milliseconds or {@code null} if parsing failed.
	 */
	private static Long parseTime(final String timeString) {
		return TextUtils.isEmpty(timeString) ? null : TimePickerDialog.TimeParser.parse(timeString);
	}

	/**
	 */
	@Override @NonNull protected TimePickerDialog.TimeOptions onCreateDialogOptions(@NonNull final Resources resources) {
		return new TimePickerDialog.TimeOptions(resources);
	}

	/**
	 */
	@Override protected void onConfigureDialogOptions(
			@NonNull final TimePickerDialog.TimeOptions options,
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super.onConfigureDialogOptions(options, context, attrs, defStyleAttr, defStyleRes);
		String formatPattern = DEFAULT_FORMAT_PATTERN;
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingTimeDialogPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingTimeDialogPreference_settingDateFormat) {
				final String dateFormat = attributes.getString(attrIndex);
				formatPattern = TextUtils.isEmpty(dateFormat) ? formatPattern : dateFormat;
			} else if (attrIndex == R.styleable.SettingTimeDialogPreference_dialogTime) {
				final Long time = parseTime(attributes.getString(attrIndex));
				if (time != null) options.time(time);
			} else if (attrIndex == R.styleable.SettingTimeDialogPreference_dialogTimePickers) {
				options.timePickers(attributes.getInt(attrIndex, options.timePickers()));
			} else if (attrIndex == R.styleable.SettingTimeDialogPreference_dialogTimeQuantityText) {
				options.timeQuantityText(attributes.getResourceId(attrIndex, 0));
			}
		}
		attributes.recycle();
		setFormat(new SimpleDateFormat(formatPattern, Locale.getDefault()));
	}

	/**
	 */
	@Override long onParseDefaultValue(@NonNull final Object defaultValue) {
		final Long time = parseTime((String) defaultValue);
		return time == null ? 0 : time;
	}

	/**
	 * Same as {@link #setTime(long)} with milliseconds value of the given <var>date</var>.
	 *
	 * @param time The desired time with milliseconds value. May be {@code null} which corresponds
	 *             to {@code 0}.
	 *
	 * @see #getTime()
	 */
	public void setTime(@Nullable final Date time) {
		setTime(time == null ? 0 : time.getTime());
	}

	/**
	 * Sets a preferred time value for this preference.
	 * <p>
	 * If value of this preference changes, it is persisted and the change listener is notified
	 * about the change.
	 *
	 * @param timeInMillis The preferred time in milliseconds to be persisted.
	 *
	 * @see #getTimeInMillis()
	 */
	public void setTime(final long timeInMillis) {
		setMilliseconds(timeInMillis);
	}

	/**
	 * Like {@link #getTimeInMillis()}, but this method may be used to check whether the time value
	 * has been specified or not, as this method will return {@code null} Date object if there is
	 * no time value specified.
	 *
	 * @return Time as date object. May be {@code null} if no time value has been specified yet.
	 *
	 * @see #setTime(Date)
	 * @see #getDialogOptions()
	 */
	@Nullable public Date getTime() {
		return areMillisecondsSet() ? new Date(getTimeInMillis()) : null;
	}

	/**
	 * Returns the preferred time value of this preference.
	 *
	 * @return Time in milliseconds either specified by the user, as default value or the persisted one.
	 *
	 * @see #setTime(long)
	 * @see #getDialogOptions()
	 */
	public long getTimeInMillis() {
		return getMilliseconds();
	}

	/**
	 * Dialog options of this preference with the preferred time specified as
	 * {@link TimePickerDialog.TimeOptions#time(long)}, if it is set.
	 *
	 * @see #getTimeInMillis()
	 */
	@Override @NonNull public TimePickerDialog.TimeOptions getDialogOptions() {
		final TimePickerDialog.TimeOptions options = super.getDialogOptions();
		if (areMillisecondsSet()) {
			options.time(getTimeInMillis());
		}
		return options;
	}

	/**
	 */
	@Override protected boolean onHandleDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
		if (dialog instanceof TimePickerDialog) {
			switch (button) {
				case Dialog.BUTTON_POSITIVE:
					final long newTime = ((TimePickerDialog) dialog).getTime();
					if (newTime != getTimeInMillis() && callChangeListener(newTime)) {
						setTime(newTime);
					}
					return true;
				default:
					return true;
			}
		}
		return super.onHandleDialogButtonClick(dialog, button);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}