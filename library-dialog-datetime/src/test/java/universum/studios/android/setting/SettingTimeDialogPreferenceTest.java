/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Build;

import org.junit.Test;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;

import androidx.preference.Preference;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.TimePickerDialog;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingTimeDialogPreferenceTest extends AndroidTestCase {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(SettingTimeDialogPreference.DEFAULT_FORMAT_PATTERN);

	@Test public void testContract() {
		// Assert:
		assertThat(SettingTimeDialogPreference.DEFAULT_FORMAT_PATTERN, is("hh:mm a"));
	}

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingTimeDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingTimeDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context(), null, R.attr.settingTimeDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getTime(), is(nullValue()));
		assertThat(preference.getTimeInMillis(), is(0L));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context(), null, R.attr.settingEditDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));

		assertThat(preference.getTime(), is(nullValue()));
		assertThat(preference.getTimeInMillis(), is(0L));
	}

	@Test public void testOnCreateDialogOptions() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		// Act:
		final TimePickerDialog.TimeOptions options = preference.onCreateDialogOptions(context().getResources());
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is(nullValue()));
		assertThat(options.content(), is(nullValue()));
		assertThat(options, is(not(preference.onCreateDialogOptions(context().getResources()))));
	}

	@Test public void testOnParseDefaultValue() throws Exception {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		// Act:
		final long parsedValue = preference.onParseDefaultValue("10:25 AM");
		// Assert:
		assertThat(parsedValue, is(DATE_FORMAT.parse("10:25 AM").getTime()));
	}

	@Test public void testTime() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		// Act + Assert:
		assertThat(preference.areMillisecondsSet(), is(false));
		preference.setTime(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		preference.setTime(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getTime(), is(notNullValue()));
		assertThat(preference.getTime().getTime(), is(1L));
		assertThat(preference.getTimeInMillis(), is(1L));
		preference.setTime(10L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getTime(), is(notNullValue()));
		assertThat(preference.getTime().getTime(), is(10L));
		assertThat(preference.getTimeInMillis(), is(10L));
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		// Act + Assert:
		assertThat(preference.getDialogOptions().time(), is(greaterThan(System.currentTimeMillis() - 100L)));
		preference.setTime(10L);
		assertThat(preference.getDialogOptions().time(), is(10L));
	}

	@Test public void testOnHandleDialogButtonClickPositive() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final TimePickerDialog mockDialog = mock(TimePickerDialog.class);
		when(mockDialog.getTime()).thenReturn(10L);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getTime();
		verifyNoMoreInteractions(mockDialog);
		verify(mockChangeListener).onPreferenceChange(preference, 10L);
		verifyNoMoreInteractions(mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickNegative() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final TimePickerDialog mockDialog = mock(TimePickerDialog.class);
		when(mockDialog.getTime()).thenReturn(10L);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_NEGATIVE), is(true));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickForNotTimePickerDialog() {
		// Arrange:
		final SettingTimeDialogPreference preference = new SettingTimeDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final Dialog mockDialog = mock(SimpleDialog.class);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}
}