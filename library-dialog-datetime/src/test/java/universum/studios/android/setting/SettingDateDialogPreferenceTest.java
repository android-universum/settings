/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.os.Build;

import org.junit.Test;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;

import androidx.preference.Preference;
import universum.studios.android.dialog.DatePickerDialog;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingDateDialogPreferenceTest extends AndroidTestCase {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(SettingDateDialogPreference.DEFAULT_FORMAT_PATTERN);

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingDateDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingDateDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context(), null, R.attr.settingDateDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getDate(), is(nullValue()));
		assertThat(preference.getDateInMillis(), is(0L));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context(), null, R.attr.settingDateDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));

		assertThat(preference.getDate(), is(nullValue()));
		assertThat(preference.getDateInMillis(), is(0L));
	}

	@Test public void testOnCreateDialogOptions() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		// Act:
		final DatePickerDialog.DateOptions options = preference.onCreateDialogOptions(context().getResources());
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is(nullValue()));
		assertThat(options.content(), is(nullValue()));
		assertThat(options, is(not(preference.onCreateDialogOptions(context().getResources()))));
	}

	@Test public void testOnParseDefaultValue() throws Exception {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		// Act:
		final long parsedValue = preference.onParseDefaultValue("2018-05-15");
		// Assert:
		assertThat(parsedValue, is(DATE_FORMAT.parse("May 15, 2018").getTime()));
	}

	@Test public void testDate() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		// Act + Assert:
		assertThat(preference.areMillisecondsSet(), is(false));
		preference.setDate(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		preference.setDate(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getDate(), is(notNullValue()));
		assertThat(preference.getDate().getTime(), is(1L));
		assertThat(preference.getDateInMillis(), is(1L));
		preference.setDate(10L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getDate(), is(notNullValue()));
		assertThat(preference.getDate().getTime(), is(10L));
		assertThat(preference.getDateInMillis(), is(10L));
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		// Act + Assert:
		assertThat(preference.getDialogOptions().date(), is(greaterThan(System.currentTimeMillis() - 100L)));
		preference.setDate(10L);
		assertThat(preference.getDialogOptions().date(), is(10L));
	}

	@Test public void testOnHandleDialogButtonClickPositive() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final DatePickerDialog mockDialog = mock(DatePickerDialog.class);
		when(mockDialog.getDate()).thenReturn(10L);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getDate();
		verifyNoMoreInteractions(mockDialog);
		verify(mockChangeListener).onPreferenceChange(preference, 10L);
		verifyNoMoreInteractions(mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickNegative() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final DatePickerDialog mockDialog = mock(DatePickerDialog.class);
		when(mockDialog.getDate()).thenReturn(10L);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_NEGATIVE), is(true));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickForNotDatePickerDialog() {
		// Arrange:
		final SettingDateDialogPreference preference = new SettingDateDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final Dialog mockDialog = mock(SimpleDialog.class);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}
}