/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.annotation.Config;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingDateTimeDialogPreferenceTest extends AndroidTestCase {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm");

	private static long parseTime(@NonNull final String value) {
		try {
			return DATE_FORMAT.parse(value).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0L;
	}

	@Test public void testInstantiationSimple() {
		// Act:
		new TestPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new TestPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingDateTimeDialogPreference preference = new TestPreference(context(), null, R.attr.settingEditDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getMilliseconds(), is(0L));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingDateTimeDialogPreference preference = new TestPreference(context(), null, R.attr.settingEditDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getMilliseconds(), is(0L));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingDateTimeDialogPreference preference = new TestPreference(context());
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getString(0)).thenReturn("10:25");
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is("10:25"));
		verify(mockTypedArray).getString(0);
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingDateTimeDialogPreference preference = new TestPreference(context());
		// Act + Assert:
		preference.onSetInitialValue(null);
		assertThat(preference.getMilliseconds(), is(not(0L)));
		preference.onSetInitialValue("11:25");
		assertThat(preference.getMilliseconds(), is(parseTime("11:25")));
	}

	@Test public void testMilliseconds() {
		// Arrange:
		final SettingDateTimeDialogPreference preference = new TestPreference(context());
		// Act + Assert:
		assertThat(preference.areMillisecondsSet(), is(false));
		preference.setMilliseconds(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		preference.setMilliseconds(1L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getMilliseconds(), is(1L));
		preference.setMilliseconds(10L);
		assertThat(preference.areMillisecondsSet(), is(true));
		assertThat(preference.getMilliseconds(), is(10L));
	}

	@Test public void testOnBindView() throws Exception {
		// Arrange:
		final SettingDateTimeDialogPreference preference = new TestPreference(context());
		preference.setFormat(DATE_FORMAT);
		preference.setSummary("Test summary.");
		final long milliseconds = System.currentTimeMillis();
		preference.setMilliseconds(milliseconds);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockSummaryView).setText(DATE_FORMAT.format(milliseconds));
		verify(mockSummaryView, times(2)).setVisibility(View.VISIBLE);
	}

	@Test public void testOnGetSummaryText() {
		// Arrange:
		final SettingDateTimeDialogPreference preference = new TestPreference(context());
		final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		preference.setFormat(dateFormat);
		preference.setSummary("Test summary.");
		final long milliseconds = System.currentTimeMillis();
		// Act + Assert:
		assertThat(preference.onGetSummaryText(), is("Test summary."));
		preference.setMilliseconds(milliseconds);
		assertThat(preference.onGetSummaryText(), is(dateFormat.format(milliseconds)));
	}

	private static final class TestPreference extends SettingDateTimeDialogPreference {

		TestPreference(@NonNull final Context context) { super(context); }
		TestPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) { super(context, attrs); }
		TestPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) { super(context, attrs, defStyleAttr); }
		TestPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
			super(context, attrs, defStyleAttr, defStyleRes);
		}

		@Override long onParseDefaultValue(@NonNull final Object defaultValue) {
			return parseTime((String) defaultValue);
		}
	}
}