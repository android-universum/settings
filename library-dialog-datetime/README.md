Settings-Dialog-DateTime
===============

This module contains dialog preference implementations that allow to set a desired **date** and
**time** setting value that may be picked via simple date/time **material** pickers.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-dialog-datetime:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core),
[settings-dialog-base](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base),
[universum.studios.android:dialogs](https://bitbucket.org/android-universum/dialogs)

Below are listed some of **primary elements** that are available in this module:

- [SettingDateDialogPreference](https://bitbucket.org/android-universum/settings/src/main/library-dialog-datetime/src/main/java/universum/studios/android/setting/SettingDateDialogPreference.java)
- [SettingTimeDialogPreference](https://bitbucket.org/android-universum/settings/src/main/library-dialog-datetime/src/main/java/universum/studios/android/setting/SettingTimeDialogPreference.java)
