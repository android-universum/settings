@Settings-Dialog
===============

This module groups the following modules into one **single group**:

- [Dialog-Base](https://github.com/universum-studios/android_settings/tree/main/library-dialog-base)
- [Dialog-Collection](https://github.com/universum-studios/android_settings/tree/main/library-dialog-collection)
- [Dialog-Color](https://github.com/universum-studios/android_settings/tree/main/library-dialog-color)
- [Dialog-DateTime](https://github.com/universum-studios/android_settings/tree/main/library-dialog-datetime)
- [Dialog-Edit](https://github.com/universum-studios/android_settings/tree/main/library-dialog-edit)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-dialog:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core),
[universum.studios.android:dialogs](https://bitbucket.org/android-universum/dialogs)