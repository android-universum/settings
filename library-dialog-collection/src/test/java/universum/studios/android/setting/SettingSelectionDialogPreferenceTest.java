/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.res.TypedArray;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.annotation.Config;

import java.util.List;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.SelectionDialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.adapter.DialogSelectionAdapter;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingSelectionDialogPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingSelectionDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingSelectionDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context(), null, R.attr.settingSelectionDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getEntries(), is(nullValue()));
		assertThat(preference.getEntryValues(), is(nullValue()));
		assertThat(preference.getSelection(), is(nullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context(), null, R.attr.settingSelectionDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getEntries(), is(nullValue()));
		assertThat(preference.getEntryValues(), is(nullValue()));
		assertThat(preference.getSelection(), is(nullValue()));
	}

	@Test public void testCreateEntryValuesFromPersistedValues() {
		// Arrange:
		final String persistedValues = "[TestValue.1, TestValue.2]";
		// Act:
		final String[] testValues = SettingSelectionDialogPreference.createEntryValuesFromPersistedValues(persistedValues);
		// Assert:
		assertThat(testValues, is(notNullValue()));
		assertThat(testValues.length, is(2));
		assertThat(testValues[0], is("TestValue.1"));
		assertThat(testValues[1], is("TestValue.2"));
	}

	@Test public void testCreateEntryValuesFromPersistedValuesThatHaveInvalidFormat() {
		// Act + Assert:
		assertThat(SettingSelectionDialogPreference.createEntryValuesFromPersistedValues("{TestValue.1, TestValue.2}"), is(nullValue()));
		assertThat(SettingSelectionDialogPreference.createEntryValuesFromPersistedValues("TestValue.1, TestValue.2"), is(nullValue()));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		preference.setEntryValues(testValues);
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getString(0)).thenReturn("TestValue.1");
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is("TestValue.1"));
		verify(mockTypedArray).getString(0);
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		// Act + Assert:
		preference.onSetInitialValue(null);
		assertThat(preference.getSelectedEntryValues(), is(nullValue()));
		preference.onSetInitialValue("[TestValue.1, TestValue.2]");
		assertThat(preference.getSelectedEntryValues(), is(new String[]{"TestValue.1", "TestValue.2"}));
	}

	@Test public void testSummaryTextBuilder() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2", "Test entry - 3"};
		preference.setEntries(testEntries);
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		final SettingSelectionDialogPreference.SummaryTextBuilder mockBuilder = mock(SettingSelectionDialogPreference.SummaryTextBuilder.class);
		preference.setSummaryTextBuilder(mockBuilder);
		// Act + Assert:
		preference.onGetSummaryText();
		verifyZeroInteractions(mockBuilder);
		preference.setSelection(new long[]{0, 2});
		preference.onGetSummaryText();
		verify(mockBuilder).clear();
		verify(mockBuilder, times(2)).appendEntry(anyString());
		verify(mockBuilder).build();
		verifyNoMoreInteractions(mockBuilder);
	}

	@Test public void testEntries() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2"};
		// Act:
		preference.setEntries(testEntries);
		final CharSequence[] entries = preference.getEntries();
		// Assert:
		assertThat(entries, is(testEntries));
	}

	@Test public void testEntriesFromResourceId() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = context().getResources().getTextArray(android.R.array.emailAddressTypes);
		// Act:
		preference.setEntries(android.R.array.emailAddressTypes);
		final CharSequence[] entries = preference.getEntries();
		// Assert:
		assertThat(entries, is(testEntries));
	}

	@Test public void testEntryValues() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2"};
		// Act:
		preference.setEntryValues(testValues);
		final String[] values = preference.getEntryValues();
		// Assert:
		assertThat(values, is(testValues));
	}

	@Test public void testEntryValuesFromResourceId() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final String[] testValues = context().getResources().getStringArray(android.R.array.emailAddressTypes);
		// Act:
		preference.setEntryValues(android.R.array.emailAddressTypes);
		final String[] values = preference.getEntryValues();
		// Assert:
		assertThat(values, is(testValues));
	}

	@Test public void testSelection() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2", "Test entry - 3"};
		preference.setEntries(testEntries);
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		// Act + Assert:
		preference.setSelection(new long[]{0, 2});
		assertThat(preference.getSelectedEntryValues(), is(new String[]{"TestValue.1", "TestValue.3"}));
		preference.setSelection(new long[]{1});
		assertThat(preference.getSelectedEntryValues(), is(new String[]{"TestValue.2"}));
	}

	@Test public void testOnBindViewHolder() throws Exception {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2", "Test entry - 3"};
		preference.setEntries(testEntries);
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		preference.setSelection(new long[]{0, 2});
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockSummaryView).setText("Test entry - 1, Test entry - 3");
		verify(mockSummaryView).setVisibility(View.VISIBLE);
	}

	@Test public void testOnGetSummaryText() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2", "Test entry - 3"};
		preference.setEntries(testEntries);
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		preference.setSummary("Test summary.");
		// Act + Assert:
		assertThat(preference.onGetSummaryText(), is("Test summary."));
		preference.setSelection(new long[]{0, 2});
		assertThat(preference.onGetSummaryText(), is("Test entry - 1, Test entry - 3"));
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final CharSequence[] testEntries = {"Test entry - 1", "Test entry - 2", "Test entry - 3"};
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		final long[] testSelection = {0, 2};
		// Act + Assert:
		assertThat(preference.getDialogOptions().items(), is(nullValue()));
		assertThat(preference.getDialogOptions().selection(), is(nullValue()));
		preference.setEntries(testEntries);
		preference.setSelection(testSelection);
		final List<DialogSelectionAdapter.Item> dialogItems = preference.getDialogOptions().items();
		assertThat(dialogItems, is(notNullValue()));
		assertThat(dialogItems.size(), is(testEntries.length));
		assertThat(dialogItems.get(0).getText(), is(testEntries[0]));
		assertThat(dialogItems.get(1).getText(), is(testEntries[1]));
		assertThat(dialogItems.get(2).getText(), is(testEntries[2]));
		assertThat(preference.getDialogOptions().selection(), is(testSelection));
	}

	@Test public void testOnHandleDialogButtonClickPositive() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final String[] testValues = {"TestValue.1", "TestValue.2", "TestValue.3"};
		preference.setEntryValues(testValues);
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final SelectionDialog mockDialog = mock(SelectionDialog.class);
		when(mockDialog.getSelection()).thenReturn(new long[]{0, 2});
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getSelection();
		verifyNoMoreInteractions(mockDialog);
		verify(mockChangeListener).onPreferenceChange(preference, new String[]{"TestValue.1", "TestValue.3"});
		verifyNoMoreInteractions(mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickNegative() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final SelectionDialog mockDialog = mock(SelectionDialog.class);
		when(mockDialog.getSelection()).thenReturn(new long[]{0, 2});
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_NEGATIVE), is(true));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickForNotColorDialog() {
		// Arrange:
		final SettingSelectionDialogPreference preference = new SettingSelectionDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final Dialog mockDialog = mock(SimpleDialog.class);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}
}