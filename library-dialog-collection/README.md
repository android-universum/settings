Settings-Dialog-Collection
===============

This module contains dialog preference implementation that allows to set a desired setting value that
may be picked from multiple values displayed **collection** either in **single** or **multiple**
selection mode.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Asettings/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Asettings/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:settings-dialog-collection:${DESIRED_VERSION}@aar"

_depends on:_
[settings-core](https://bitbucket.org/android-universum/settings/src/main/library-core),
[settings-dialog-base](https://bitbucket.org/android-universum/settings/src/main/library-dialog-base),
[universum.studios.android:dialogs](https://bitbucket.org/android-universum/dialogs)

Below are listed some of **primary elements** that are available in this module:

- [SettingSelectionDialogPreference](https://bitbucket.org/android-universum/settings/src/main/library-dialog-collection/src/main/java/universum/studios/android/setting/SettingSelectionDialogPreference.java)
