/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.setting.R;

/**
 * A simple {@link View} implementation that draws a specified color in a circular shape. The
 * color to be drawn may be specified via {@link #setColor(int)}. Also there may be specified a
 * color via {@link #setCanvasColor(int)} that should be drawn behind the primary color.
 *
 * <h3>Xml attributes</h3>
 * See {@link View},
 * {@link R.styleable#SettingColorView SettingColorView Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingColorViewStyle settingColorViewStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SettingColorView extends View {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingColorView";

	/**
	 * Class name for accessibility events.
	 */
	@VisibleForTesting static final String ACCESSIBILITY_EVENT_CLASS_NAME = "universum.studios.android.setting.widget.ColorView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Pain used to draw graphics of this view.
	 */
	private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

	/**
	 * Raw color specified via {@link #setCanvasColor(int)}. This color is used as base for color
	 * used to draw canvas/background graphics of this view.
	 */
	private int rawCanvasColor = Color.WHITE;

	/**
	 * Modified {@link #rawCanvasColor} by the current alpha value of this view.
	 */
	private int drawCanvasColor = rawCanvasColor;

	/**
	 * Raw color specified via {@link #setColor(int)}. This color is used as base for color used to
	 * draw primary graphics of this view.
	 */
	private int rawColor = Color.WHITE;

	/**
	 * Modified {@link #rawColor} by the current alpha value of this view.
	 */
	private int drawColor = rawColor;

	/**
	 * Maximum size dimension of this view.
	 */
	private int maxWidth, mMaxHeight;

	/**
	 * Size dimension of this view.
	 */
	private int width, height;

	/**
	 * Radius used to draw circular graphics of this view.
	 */
	private float radius;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingColorView(Context, AttributeSet)} without attributes.
	 */
	public SettingColorView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingColorView(Context, AttributeSet, int)} with {@link R.attr#settingColorViewStyle}
	 * as attribute for default style.
	 */
	public SettingColorView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingColorViewStyle);
	}

	/**
	 * Same as {@link #SettingColorView(Context, AttributeSet, int, int)} with {@code 0} as default style.
	 */
	public SettingColorView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of SettingColorView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingColorView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	@SuppressLint("CustomViewStyleable")
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.maxWidth = mMaxHeight = Integer.MAX_VALUE;
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingColorView, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingColorView_android_color) {
				setColor(attributes.getColor(attrIndex, rawColor));
			} else if (attrIndex == R.styleable.SettingColorView_android_maxWidth) {
				this.maxWidth = attributes.getDimensionPixelSize(attrIndex, maxWidth);
			} else if (attrIndex == R.styleable.SettingColorView_android_maxHeight) {
				this.mMaxHeight = attributes.getDimensionPixelSize(attrIndex, mMaxHeight);
			}
		}
		attributes.recycle();
	}

	/**
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override public void onInitializeAccessibilityEvent(@NonNull final AccessibilityEvent event) {
		super.onInitializeAccessibilityEvent(event);
		event.setClassName(ACCESSIBILITY_EVENT_CLASS_NAME);
	}

	/**
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override public void onInitializeAccessibilityNodeInfo(@NonNull final AccessibilityNodeInfo info) {
		super.onInitializeAccessibilityNodeInfo(info);
		info.setClassName(ACCESSIBILITY_EVENT_CLASS_NAME);
	}

	/**
	 * Sets a color to be drawn behind the primary color of this color view.
	 *
	 * @param color The desired color.
	 *
	 * @see #getCanvasColor()
	 * @see #setColor(int)
	 */
	public void setCanvasColor(@ColorInt final int color) {
		if (rawCanvasColor != color || drawCanvasColor != color) {
			this.rawCanvasColor = drawCanvasColor = color;
			invalidate();
		}
	}

	/**
	 * Returns the current color drawn behind the primary color by this color view.
	 *
	 * @return Current canvas color.
	 */
	@ColorInt public int getCanvasColor() {
		return drawCanvasColor;
	}

	/**
	 * Sets a color to be drawn by this color view.
	 *
	 * @param color The desired color.
	 *
	 * @see #getColor()
	 */
	public void setColor(@ColorInt final int color) {
		if (rawColor != color || drawColor != color) {
			this.rawColor = drawColor = color;
			invalidate();
		}
	}

	/**
	 * Returns the current color drawn by this color view.
	 *
	 * @return Current color.
	 *
	 * @see #setColor(int)
	 */
	@ColorInt public int getColor() {
		return drawColor;
	}

	/**
	 */
	@Override protected boolean onSetAlpha(final int alpha) {
		this.drawColor = (rawColor << 8 >>> 8) | (alpha << 24);
		this.drawCanvasColor = (drawCanvasColor << 8 >>> 8) | (alpha << 24);
		return true;
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		// Take into count maximum size.
		width = Math.min(width, maxWidth);
		height = Math.min(height, mMaxHeight);
		setMeasuredDimension(width, height);
	}

	/**
	 */
	@Override protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
		super.onSizeChanged(width, height, oldWidth, oldHeight);
		this.width = width;
		this.height = height;
		this.radius = Math.min(this.width, this.height) / 2f;
	}

	/**
	 */
	@Override protected void onDraw(@NonNull final Canvas canvas) {
		super.onDraw(canvas);
		if ((drawCanvasColor >>> 24) != 0) {
			this.paint.setColor(rawCanvasColor);
			canvas.drawCircle(width / 2f, height / 2f, radius, paint);
		}
		if ((drawColor >>> 24) != 0) {
			this.paint.setColor(drawColor);
			canvas.drawCircle(width / 2f, height / 2f, radius, paint);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}