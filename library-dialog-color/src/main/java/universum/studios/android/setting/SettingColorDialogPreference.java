/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.ColorPickerDialog;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.setting.widget.SettingColorView;

/**
 * A {@link SettingDialogPreference} implementation that may be used to allow to a user to pick
 * its preferred color for a specific setting preference.
 * <p>
 * This preference implementation by default displays the preferred color via {@link SettingColorView}
 * widget in the widget area (at the end of layout hierarchy). The preferred color may be specified
 * via {@link #setColor(int)} and obtained via {@link #getColor()}.
 * <p>
 * When {@link #handleDialogButtonClick(Dialog, int)} is called, this preference implementation
 * handles only {@link ColorPickerDialog} type of dialog. If its {@link Dialog#BUTTON_POSITIVE} button
 * has been clicked, the color provided via {@link ColorPickerDialog#getColor()} is set as color for
 * this preference via {@link #setColor(int)}.
 *
 * <h3>Default value</h3>
 * Default value for this preference is parsed as color {@link Integer}. See {@link TypedArray#getColor(int, int)}.
 *
 * <h3>Xml attributes</h3>
 * See {@link SettingDialogPreference},
 * {@link R.styleable#SettingColorDialogPreference SettingColorDialogPreference Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#settingColorDialogPreferenceStyle settingColorDialogPreferenceStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("RedundantCast")
public final class SettingColorDialogPreference extends SettingDialogPreference<ColorPickerDialog.ColorOptions> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SettingColorDialogPreference";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Boolean flag indicating whether the color value for this preference has been set or not.
	 * This flag is used to handle case when the same value is being specified for this preference,
	 * but for the first time, to properly refresh view of this preference and notify listeners about
	 * the change.
	 */
	private boolean colorSet;

	/**
	 * Current color value specified for this preference. This may be either value specified by the
	 * user, default value or persisted value.
	 */
	private int color = Color.TRANSPARENT;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SettingColorDialogPreference(Context, AttributeSet)} without attributes.
	 */
	public SettingColorDialogPreference(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SettingColorDialogPreference(Context, AttributeSet, int)} with
	 * {@link R.attr#settingColorDialogPreferenceStyle} as attribute for default style.
	 */
	public SettingColorDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.settingColorDialogPreferenceStyle);
	}

	/**
	 * Same as {@link #SettingColorDialogPreference(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public SettingColorDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of SettingColorDialogPreference for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new setting preference presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this preference.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this preference within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new preference.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SettingColorDialogPreference(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @NonNull protected ColorPickerDialog.ColorOptions onCreateDialogOptions(@NonNull final Resources resources) {
		return new ColorPickerDialog.ColorOptions(resources);
	}

	/**
	 */
	@Override protected void onConfigureDialogOptions(
			@NonNull final ColorPickerDialog.ColorOptions options,
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super.onConfigureDialogOptions(options, context, attrs, defStyleAttr, defStyleRes);
		int canvasColor = options.canvasColor();
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SettingColorDialogPreference, defStyleAttr, defStyleRes);
		final int attributeCount = attributes.getIndexCount();
		for (int i = 0; i < attributeCount; i++) {
			final int attrIndex = attributes.getIndex(i);
			if (attrIndex == R.styleable.SettingColorDialogPreference_dialogColor) {
				options.color(attributes.getColor(attrIndex, options.color()));
			} else if (attrIndex == R.styleable.SettingColorDialogPreference_dialogColorCanvas) {
				canvasColor = attributes.getColor(attrIndex, canvasColor);
			}
		}
		attributes.recycle();
		options.canvasColor(canvasColor);
	}

	/**
	 */
	@Override protected Object onGetDefaultValue(@NonNull final TypedArray attributes, final int index) {
		return attributes.getColor(index, color);
	}

	/**
	 */
	@Override protected void onSetInitialValue(@Nullable final Object defaultValue) {
		setColor(getPersistedInt(defaultValue == null ? Color.TRANSPARENT : (int) defaultValue));
	}

	/**
	 * Sets a preferred color value for this preference.
	 * <p>
	 * If value of this preference changes, it is persisted and the change listener is notified
	 * about the change.
	 *
	 * @param color The preferred color to be persisted as {@link Integer}.
	 *
	 * @see #getColor()
	 */
	public void setColor(@ColorInt final int color) {
		final boolean changed = this.color != color;
		if (changed || !colorSet) {
			this.color = color;
			this.colorSet = true;
			persistInt(this.color);
			if (changed) {
				notifyChanged();
			}
		}
	}

	/**
	 * Returns the preferred color value of this preference.
	 *
	 * @return Color integer either specified by the user, as default value or the persisted one.
	 *
	 * @see #setColor(int)
	 * @see #getDialogOptions()
	 */
	@ColorInt public int getColor() {
		return color;
	}

	/**
	 */
	@Override public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);
		final SettingColorView colorView = (SettingColorView) holder.findViewById(R.id.setting_color_view);
		if (colorView != null) {
			colorView.setCanvasColor(getDialogOptions().canvasColor());
			colorView.setColor(color);
		}
	}

	/**
	 * Dialog options of this preference with the preferred color specified as
	 * {@link ColorPickerDialog.ColorOptions#color(int)}, if it is set.
	 *
	 * @see #getColor()
	 */
	@Override @NonNull public ColorPickerDialog.ColorOptions getDialogOptions() {
		final ColorPickerDialog.ColorOptions options = super.getDialogOptions();
		if (colorSet) {
			options.color(color);
		}
		return options;
	}

	/**
	 */
	@Override protected boolean onHandleDialogButtonClick(@NonNull final Dialog dialog, @Dialog.Button final int button) {
		if (dialog instanceof ColorPickerDialog) {
			switch (button) {
				case Dialog.BUTTON_POSITIVE:
					final int newColor = ((ColorPickerDialog) dialog).getColor();
					if (newColor != getColor() && callChangeListener(newColor)) {
						setColor(newColor);
					}
					return true;
				default:
					return true;
			}
		}
		return super.onHandleDialogButtonClick(dialog, button);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}