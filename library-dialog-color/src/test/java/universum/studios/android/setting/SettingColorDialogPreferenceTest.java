/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.widget.TextView;

import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import universum.studios.android.dialog.ColorPickerDialog;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.setting.test.TestPreferenceViewHolders;
import universum.studios.android.setting.widget.SettingColorView;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SettingColorDialogPreferenceTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingColorDialogPreference(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingColorDialogPreference(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context(), null, R.attr.settingColorDialogPreferenceStyle);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getColor(), is(Color.TRANSPARENT));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context(), null, R.attr.settingColorDialogPreferenceStyle, 0);
		// Assert:
		assertThat(preference.getDialogId(), is(SettingDialogPreference.NO_DIALOG_ID));
		assertThat(preference.getDialogOptions(), is(notNullValue()));
		assertThat(preference.getColor(), is(Color.TRANSPARENT));
	}

	@Test public void testOnCreateDialogOptions() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		preference.setTitle("Test title");
		// Act:
		final ColorPickerDialog.ColorOptions options = preference.onCreateDialogOptions(context().getResources());
		// Assert:
		assertThat(options, is(notNullValue()));
		assertThat(options.title(), is(nullValue()));
		assertThat(options.content(), is(nullValue()));
		assertThat(options, is(not(preference.onCreateDialogOptions(context().getResources()))));
	}

	@Test public void testOnGetDefaultValue() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockTypedArray.getColor(0, preference.getColor())).thenReturn(Color.RED);
		// Act:
		final Object defaultValue = preference.onGetDefaultValue(mockTypedArray, 0);
		// Assert:
		assertThat(defaultValue, is(Color.RED));
		verify(mockTypedArray).getColor(0, preference.getColor());
		verifyNoMoreInteractions(mockTypedArray);
	}

	@Test public void testOnSetInitialValue() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		// Act + Assert:
		preference.onSetInitialValue(null);
		assertThat(preference.getColor(), is(Color.TRANSPARENT));
		preference.onSetInitialValue(Color.RED);
		assertThat(preference.getColor(), is(Color.RED));
	}

	@Test public void testColor() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		// Act + Assert:
		preference.setColor(Color.RED);
		preference.setColor(Color.RED);
		assertThat(preference.getColor(), is(Color.RED));
		preference.setColor(Color.BLUE);
		assertThat(preference.getColor(), is(Color.BLUE));
	}

	@Test public void testOnBindView() throws Exception {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		preference.setColor(Color.RED);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final SettingColorView mockColorView = mock(SettingColorView.class);
		when(mockViewHolder.findViewById(R.id.setting_color_view)).thenReturn(mockColorView);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(R.id.setting_color_view);
		verify(mockColorView).setCanvasColor(preference.getDialogOptions().canvasColor());
		verify(mockColorView).setColor(Color.RED);
		verifyNoMoreInteractions(mockColorView);
	}

	@Test public void testOnBindViewWithoutColorViewInHierarchy() throws Exception {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		preference.setColor(Color.RED);
		final PreferenceViewHolder mockViewHolder = TestPreferenceViewHolders.createMockHolder(context());
		final TextView mockSummaryView = mock(TextView.class);
		when(mockViewHolder.findViewById(android.R.id.summary)).thenReturn(mockSummaryView);
		// Act:
		preference.onBindViewHolder(mockViewHolder);
		// Assert:
		verify(mockViewHolder).findViewById(R.id.setting_color_view);
	}

	@Test public void testDialogOptions() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		// Act + Assert:
		assertThat(preference.getDialogOptions().color(), is(Color.TRANSPARENT));
		preference.setColor(Color.RED);
		assertThat(preference.getDialogOptions().color(), is(Color.RED));
	}

	@Test public void testOnHandleDialogButtonClickPositive() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final ColorPickerDialog mockDialog = mock(ColorPickerDialog.class);
		when(mockDialog.getColor()).thenReturn(Color.RED);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(true));
		verify(mockDialog).getColor();
		verifyNoMoreInteractions(mockDialog);
		verify(mockChangeListener).onPreferenceChange(preference, Color.RED);
		verifyNoMoreInteractions(mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickNegative() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final ColorPickerDialog mockDialog = mock(ColorPickerDialog.class);
		when(mockDialog.getColor()).thenReturn(Color.RED);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_NEGATIVE), is(true));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}

	@Test public void testOnHandleDialogButtonClickForNotColorDialog() {
		// Arrange:
		final SettingColorDialogPreference preference = new SettingColorDialogPreference(context());
		final Preference.OnPreferenceChangeListener mockChangeListener = mock(Preference.OnPreferenceChangeListener.class);
		preference.setOnPreferenceChangeListener(mockChangeListener);
		final Dialog mockDialog = mock(SimpleDialog.class);
		// Act + Assert:
		assertThat(preference.onHandleDialogButtonClick(mockDialog, Dialog.BUTTON_POSITIVE), is(false));
		verifyZeroInteractions(mockDialog, mockChangeListener);
	}
}