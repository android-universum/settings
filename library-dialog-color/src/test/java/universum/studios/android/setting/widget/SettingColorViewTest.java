/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.setting.widget;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import org.junit.Test;
import org.robolectric.annotation.Config;

import universum.studios.android.setting.R;
import universum.studios.android.test.AndroidTestCase;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings({"NumericOverflow"})
public final class SettingColorViewTest extends AndroidTestCase {

	@Test public void testInstantiationSimple() {
		// Act:
		new SettingColorView(context());
	}

	@Test public void testInstantiationWithAttributes() {
		// Act:
		new SettingColorView(context(), null);
	}

	@Test public void testInstantiationWithAttributesAndAttrStyle() {
		// Act:
		final SettingColorView view = new SettingColorView(context(), null, R.attr.settingColorViewStyle);
		// Assert:
		assertThat(view.getCanvasColor(), is(Color.WHITE));
		assertThat(view.getColor(), is(Color.WHITE));
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationWithAttributesAndAttrStyleAndDefaultStyle() {
		// Act:
		final SettingColorView view = new SettingColorView(context(), null, R.attr.settingColorViewStyle, 0);
		// Assert:
		assertThat(view.getCanvasColor(), is(Color.WHITE));
		assertThat(view.getColor(), is(Color.WHITE));
	}

	@Test public void testOnInitializeAccessibilityEvent() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		final AccessibilityEvent event = AccessibilityEvent.obtain();
		// Act:
		view.onInitializeAccessibilityEvent(event);
		// Assert:
		assertEquals(event.getClassName(), SettingColorView.ACCESSIBILITY_EVENT_CLASS_NAME);
	}

	@Test public void testOnInitializeAccessibilityNodeInfo() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		final AccessibilityNodeInfo info = AccessibilityNodeInfo.obtain();
		// Act:
		view.onInitializeAccessibilityNodeInfo(info);
		// Assert:
		assertEquals(info.getClassName(), SettingColorView.ACCESSIBILITY_EVENT_CLASS_NAME);
	}

	@Test public void testCanvasColor() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		// Act + Assert:
		view.setCanvasColor(Color.BLACK);
		assertThat(view.getCanvasColor(), is(Color.BLACK));
		view.setCanvasColor(Color.BLACK);
		assertThat(view.getCanvasColor(), is(Color.BLACK));
	}

	@Test public void testCanvasColorWithAppliedAlpha() {
		// Arrange:
		final int color = Color.BLACK;
		final SettingColorView view = new SettingColorView(context());
		view.setCanvasColor(color);
		view.setAlpha(0.5f);
		// Act + Assert:
		assertThat(view.getCanvasColor(), is((color << 8 >>> 8) | ((255 / 2) << 24)));
	}

	@Test public void testColor() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		// Act + Assert:
		view.setColor(Color.BLACK);
		assertThat(view.getColor(), is(Color.BLACK));
		view.setColor(Color.BLACK);
		assertThat(view.getColor(), is(Color.BLACK));
	}

	@Test public void testColorWithAppliedAlpha() {
		// Arrange:
		final int color = Color.BLACK;
		final SettingColorView view = new SettingColorView(context());
		view.setColor(color);
		view.setAlpha(0.5f);
		// Act + Assert:
		assertThat(view.getColor(), is((color << 8 >>> 8) | ((255 / 2) << 24)));
	}

	@Test public void testOnMeasure() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		// Act:
		view.onMeasure(
				View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
				View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
		);
		// Assert:
		assertThat(view.getMeasuredWidth(), is(0));
		assertThat(view.getMeasuredHeight(), is(0));
	}

	@Test public void testOnDraw() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		view.onSizeChanged(100, 100, 0, 0);
		final Canvas mockCanvas = mock(Canvas.class);
		// Act:
		view.onDraw(mockCanvas);
		// Assert:
		verify(mockCanvas, times(2)).drawCircle(eq(50f), eq(50f), eq(50f), any(Paint.class));
		verifyNoMoreInteractions(mockCanvas);
	}

	@Test public void testOnDrawWithAppliedZeroAlpha() {
		// Arrange:
		final SettingColorView view = new SettingColorView(context());
		view.setAlpha(0f);
		view.onSizeChanged(100, 100, 0, 0);
		final Canvas mockCanvas = mock(Canvas.class);
		// Act:
		view.onDraw(mockCanvas);
		// Assert:
		verifyZeroInteractions(mockCanvas);
	}
}