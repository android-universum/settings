Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.1.3](https://bitbucket.org/android-universum/settings/wiki/version/1.x) ###
> 10.05.2020

- Regular **maintenance**.

### [1.1.2](https://bitbucket.org/android-universum/settings/wiki/version/1.x) ###
> 08.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.1.1](https://bitbucket.org/android-universum/settings/wiki/version/1.x) ###
> 03.02.2019

- Updated to use [Dialogs](https://bitbucket.org/android-universum/dialogs) library in version which uses `Lifecycle` to ensure safe execution of `DialogRequests`.

### [1.1.0](https://bitbucket.org/android-universum/settings/wiki/version/1.x) ###
> 21.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.0.0](https://bitbucket.org/android-universum/settings/wiki/version/1.x) ###
> 14.05.2018

- First production release.

## [Version 0.x](https://bitbucket.org/android-universum/settings/wiki/version/0.x) ##